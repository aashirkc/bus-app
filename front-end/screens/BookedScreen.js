import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class BookedPage extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    static navigationOptions = {
        title: 'Home',
    };

    render() {
        return (
            <View style={styles.screen}>
                <Text>My Booking Screen !!!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default BookedPage;
