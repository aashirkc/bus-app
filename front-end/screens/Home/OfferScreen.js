import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class OffersPage extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <View style={styles.screen}>
                <Text>Offers Screen !!!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default OffersPage;
