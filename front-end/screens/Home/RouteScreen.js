import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { RoutesData } from '../../data/carousel-data';

class RoutesPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            RData: [],
        };
    }

    componentDidMount() {
        try {
            const routeId = this.props.navigation.getParam('routeId');
            const RData = RoutesData.find(route => route.id === routeId);
            this.setState({ RData });
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    render() {
        const { RData } = this.state;
        return (
            <View style={styles.screen}>
                <Text>{RData.routename}</Text>
                <Text>
                    {RData.start} - {RData.destination}dsf
                </Text>
            </View>
        );
    }
}

RoutesPage.navigationOptions = navigationData => {
    const routeId = navigationData.navigation.getParam('routeId');
    const RData = RoutesData.find(route => route.id === routeId);
    return {
        headerTitle: `${RData.start} - ${RData.destination}`,
    };
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default RoutesPage;
