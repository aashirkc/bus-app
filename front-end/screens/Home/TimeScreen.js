import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class TimePage extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <View style={styles.screen}>
                <Text>Time Screen !!!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default TimePage;
