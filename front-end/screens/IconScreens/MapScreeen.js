import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator } from 'react-native';
import MapView from 'react-native-maps';

import { MaterialIcons } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import * as theme from '../../constants/theme';

import Buttons from '../../components/utils/button';

export default class MapScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mapRegion: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0,
            },
            hasLocationPermissions: false,
            locationResult: null,
            errors: false,
        };
    }

    componentDidMount() {
        this._getLocationAsync();
    }

    handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };

    _getLocationAsync = async () => {
        try {
            let { status } = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                this.setState({
                    locationResult: 'Permission to access location was denied',
                });
            } else {
                this.setState({ hasLocationPermissions: true, errors: false });
            }

            let location = await Location.getCurrentPositionAsync({});
            this.setState({
                locationResult: JSON.stringify(location),
            });

            // Center the map on the location we just fetched.
            this.setState({
                mapRegion: {
                    latitude: Number(location.coords.latitude),
                    longitude: Number(location.coords.longitude),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                },
            });
        } catch (err) {
            this.setState({ errors: err });
        }
    };

    render() {
        const {
            errors,
            locationResult,
            hasLocationPermissions,
            mapRegion,
        } = this.state;
        return (
            <View style={styles.container}>
                {errors ? (
                    <>
                        <Text style={styles.mapText}>
                            Please Grant the Location Permissions to access
                            google maps.
                        </Text>
                        <Buttons
                            title="Enable it"
                            component={MaterialIcons}
                            icon={'error-outline'}
                            clickHandler={this._getLocationAsync}
                        />
                    </>
                ) : locationResult === null ? (
                    <>
                        <ActivityIndicator
                            size="large"
                            color={theme.colors.tertiaryColor}
                        />
                        <Text style={styles.mapText}>
                            Getting Your Location...
                        </Text>
                    </>
                ) : hasLocationPermissions === false ? (
                    <Text style={styles.mapText}>
                        Location permissions are not granted.
                    </Text>
                ) : mapRegion === null ? (
                    <Text style={styles.mapText}>
                        Map region doesn't exist.
                    </Text>
                ) : (
                    <MapView
                        style={{
                            alignSelf: 'stretch',
                            height: '100%',
                        }}
                        region={mapRegion}
                        // onRegionChange={this.handleMapRegionChange}
                    >
                        <MapView.Marker
                            coordinate={mapRegion}
                            title="Your Current Location"
                            // description="Your Current Location"
                        />
                    </MapView>
                )}
            </View>
        );
    }
}

MapScreen.navigationOptions = () => {
    return {
        headerTitle: "Google's Map",
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
    },
    mapText: {
        fontFamily: 'primary-regular',
        padding: 20,
        textAlign: 'center',
        color: theme.colors.headerTitle,
    },
});
