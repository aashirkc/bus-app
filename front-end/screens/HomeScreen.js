import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    ScrollView,
    Text,
    ActivityIndicator,
} from 'react-native';
import * as theme from '../constants/theme';

import Search from '../components/search';
import Articles from '../components/carousel';
import HomeIcon from '../components/HomeIcon';
import PopularRoutes from '../components/PopularRoutes';
import { Images } from '../data/carousel-data';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ImageData: [],
            loading: true,
            error: {},
        };
    }

    navigationMain = (name, id) => {
        if (id) {
            this.props.navigation.navigate({
                routeName: name,
                params: { busId: id },
            });
        }
        this.props.navigation.navigate({
            routeName: name,
        });
    };

    componentDidMount() {
        
        try {
            this.setState(prevState => ({
                ImageData: Images,
                loading: !prevState.loading,
            }));
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    render() {
        const { ImageData, loading } = this.state;
        return (
            <>
                {loading ? (
                    <ActivityIndicator
                        size="large"
                        color={theme.colors.tertiaryColor}
                    />
                ) : (
                    <TouchableWithoutFeedback>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            style={styles.screen}
                            scrollEventThrottle={16}
                        >
                            <View style={styles.Home}>
                                <Search />
                                <Articles
                                    Clicked={() =>
                                        this.navigationMain('OfferScreen')
                                    }
                                    ImageData={ImageData}
                                />
                            </View>
                            <HomeIcon Clicked={this.navigationMain} />
                        
                            <PopularRoutes
                                onClicked={id =>
                                    this.navigationMain('BusList', id)
                                }
                            />
                          
                        </ScrollView>
                    </TouchableWithoutFeedback>
                )}
            </>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: theme.colors.gray2,
        marginTop: 23,
        // backgroundColor: theme.colors.lightgray,
    },
    Home: {
        marginHorizontal: 5,
    },
});

export default HomePage;
