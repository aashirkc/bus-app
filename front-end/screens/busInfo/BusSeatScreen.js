import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    ActivityIndicator,
    TouchableNativeFeedback,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import * as theme from '../../constants/theme';
import { BusSeat } from '../../data/carousel-data';
import Buttons from '../../components/utils/button';
import { styles } from '../../assets/css/busSeatScreen';

function chunks(arr, size = 2) {
    return arr
        .map((x, i) => i % size == 0 && arr.slice(i, i + size))
        .filter(x => x);
}

class SeatScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            col1: [],
            col2: [],
            col3: [],
            selectedSeat: [],
            loading: true,
            errors: null,
        };
    }

    componentDidMount() {
        try {
            this.setState(prevState => ({
                col1: chunks(BusSeat.col1),
                col2: chunks(BusSeat.col2),
                col3: BusSeat.col3,
                loading: !prevState.loading,
            }));
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    _onPressButton = (value, col) => {
        const { col1, col2, col3, selectedSeat } = this.state;

        if (col === 'col1') {
            let newSeat = col1.map(obj => {
                return obj.map(newObj => {
                    const original = newObj.name === value.name;
                    if (original) {
                        return { ...newObj, isAvailable: !newObj.isAvailable };
                    } else return newObj;
                });
            });
            this.setState({ col1: newSeat });
        } else if (col === 'col2') {
            let newSeat = col2.map(obj => {
                return obj.map(newObj => {
                    const original = newObj.name === value.name;
                    if (original) {
                        return { ...newObj, isAvailable: !newObj.isAvailable };
                    } else return newObj;
                });
            });
            this.setState({ col2: newSeat });
        } else if (col === 'col3') {
            let newSeat = col3.map(obj => {
                const original = obj.name === value.name;
                if (original) {
                    return { ...obj, isAvailable: !obj.isAvailable };
                } else return obj;
            });
            this.setState({ col3: newSeat });
        }

        this.setState(prevstate => {
            const prevSelectedSeat = prevstate.selectedSeat;
            let retselectedSeat;
            if (prevSelectedSeat.find(rw => rw === value.name)) {
                retselectedSeat = prevSelectedSeat.filter(
                    row => row !== value.name
                );
            } else {
                retselectedSeat = prevSelectedSeat.concat(value.name);
            }
            return {
                selectedSeat: retselectedSeat,
            };
        });
    };

    availableColor = value => {
        let disColors = value && value.isbooked;
        let colors = value && value.isAvailable;

        if (disColors) {
            return theme.colors.booked;
        } else {
            let newColor =
                colors === true ? theme.colors.selected : theme.colors.empty;
            return newColor;
        }
    };

    availableTextColor = value => {
        let newTextColor;
        if (value.isbooked === true) {
            newTextColor = theme.colors.white;
            return newTextColor;
        } else if (value.isAvailable === true) {
            newTextColor = theme.colors.white;
            return newTextColor;
        } else {
            newTextColor = theme.colors.lightgray;
            return newTextColor;
        }
    };

    render() {
        const { col1, col2, col3, loading, errors, selectedSeat } = this.state;

        return (
            <>
                {errors ? (
                    <View>
                        <Text>Errors : {errors}</Text>
                    </View>
                ) : loading ? (
                    <ActivityIndicator
                        size="large"
                        color={theme.colors.tertiaryColor}
                    />
                ) : (
                    <ScrollView>
                        <View style={styles.screen}>
                            <View>
                                <View style={styles.seatInfo}>
                                    <View style={styles.seatInfoWrapper}>
                                        <View
                                            style={{
                                                ...styles.seatInfoBox,
                                                backgroundColor:
                                                    theme.colors.empty,
                                            }}
                                        />
                                        <Text style={styles.seatInfoText}>
                                            Empty
                                        </Text>
                                    </View>
                                    <View style={styles.seatInfoWrapper}>
                                        <View
                                            style={{
                                                ...styles.seatInfoBox,
                                                backgroundColor:
                                                    theme.colors.selected,
                                            }}
                                        />
                                        <Text style={styles.seatInfoText}>
                                            Selected
                                        </Text>
                                    </View>
                                    <View style={styles.seatInfoWrapper}>
                                        <View
                                            style={{
                                                ...styles.seatInfoBox,
                                                backgroundColor:
                                                    theme.colors.booked,
                                            }}
                                        />
                                        <Text style={styles.seatInfoText}>
                                            Booked
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.seatMain}>
                                    <View style={styles.seatMainWrapper}>
                                        <View style={styles.seatImageWrapper}>
                                            <Image
                                                source={require('../../assets/images/steering.jpg')}
                                                style={styles.seatImage}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.busSeatWrapper}>
                                        <View style={styles.seatStart}>
                                            {col1 &&
                                                col1.map((m, index) => (
                                                    <View
                                                        style={styles.seatSide}
                                                        key={index}
                                                    >
                                                        {m.map(
                                                            (seat, index) => (
                                                                <TouchableNativeFeedback
                                                                    key={index}
                                                                    background={TouchableNativeFeedback.Ripple(
                                                                        '#ffffff6b',
                                                                        true
                                                                    )}
                                                                    onPress={() =>
                                                                        !seat.isbooked &&
                                                                        this._onPressButton(
                                                                            seat,
                                                                            'col1'
                                                                        )
                                                                    }
                                                                >
                                                                    <View
                                                                        style={{
                                                                            ...styles.seatBox,
                                                                            backgroundColor: this.availableColor(
                                                                                seat
                                                                            ),
                                                                        }}
                                                                    >
                                                                        <Text
                                                                            style={{
                                                                                color: this.availableTextColor(
                                                                                    seat
                                                                                ),
                                                                            }}
                                                                        >
                                                                            {
                                                                                seat.name
                                                                            }
                                                                        </Text>
                                                                    </View>
                                                                </TouchableNativeFeedback>
                                                            )
                                                        )}
                                                        <Text>{'\n'}</Text>
                                                    </View>
                                                ))}
                                        </View>
                                        <View style={styles.seatEnd}>
                                            {col2.map((m, index) => (
                                                <View
                                                    style={styles.seatSide}
                                                    key={index}
                                                >
                                                    {m.map((seat, index) => (
                                                        <TouchableNativeFeedback
                                                            key={index}
                                                            background={TouchableNativeFeedback.Ripple(
                                                                '#ffffff6b',
                                                                true
                                                            )}
                                                            onPress={() =>
                                                                !seat.isbooked &&
                                                                this._onPressButton(
                                                                    seat,
                                                                    'col2'
                                                                )
                                                            }
                                                        >
                                                            <View
                                                                style={{
                                                                    ...styles.seatBox,
                                                                    backgroundColor: this.availableColor(
                                                                        seat
                                                                    ),
                                                                }}
                                                            >
                                                                <Text
                                                                    style={{
                                                                        color: this.availableTextColor(
                                                                            seat
                                                                        ),
                                                                    }}
                                                                >
                                                                    {seat.name}
                                                                </Text>
                                                            </View>
                                                        </TouchableNativeFeedback>
                                                    ))}
                                                    <Text>{'\n'}</Text>
                                                </View>
                                            ))}
                                        </View>
                                    </View>
                                    <View style={styles.busSeatWrapperlast}>
                                        {col3.map((seat, index) => (
                                            <TouchableNativeFeedback
                                                key={index}
                                                background={TouchableNativeFeedback.Ripple(
                                                    '#ffffff6b',
                                                    true
                                                )}
                                                onPress={() =>
                                                    !seat.isbooked &&
                                                    this._onPressButton(
                                                        seat,
                                                        'col3'
                                                    )
                                                }
                                            >
                                                <View
                                                    style={{
                                                        ...styles.seatBox,
                                                        backgroundColor: this.availableColor(
                                                            seat
                                                        ),
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: this.availableTextColor(
                                                                seat
                                                            ),
                                                        }}
                                                    >
                                                        {seat.name}
                                                    </Text>
                                                </View>
                                            </TouchableNativeFeedback>
                                        ))}
                                    </View>
                                </View>
                                <View
                                    style={{
                                        ...styles.seatMain,
                                        marginBottom: 20,
                                    }}
                                >
                                    <View style={styles.selectedSeat}>
                                        <View>
                                            <Text style={styles.seatInfoText}>
                                                Ticket Price
                                            </Text>
                                            <Text
                                                style={styles.selectedSeatText}
                                            >
                                                NRs. 200
                                            </Text>
                                        </View>
                                        <View>
                                            <Text style={styles.seatInfoText}>
                                                Total Price
                                            </Text>
                                            <Text
                                                style={styles.selectedSeatText}
                                            >
                                                NRs. {selectedSeat.length * 200}
                                            </Text>
                                        </View>
                                        <View>
                                            <Text style={styles.seatInfoText}>
                                                Selected Seat
                                            </Text>
                                            <Text
                                                style={styles.selectedSeatText}
                                            >
                                                {selectedSeat.length} Seat
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.selectedSeat}>
                                        <View>
                                            <Text style={styles.seatInfoText}>
                                                Passenger Name
                                            </Text>
                                            <Text
                                                style={styles.selectedSeatText}
                                            >
                                                Basanta Pandu
                                            </Text>
                                        </View>
                                        <Buttons
                                            title="Pay Now"
                                            icon="ios-wallet"
                                            component={Ionicons}
                                            clickHandler={() =>
                                                this.props.navigation.navigate(
                                                    'Payment'
                                                )
                                            }
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                )}
            </>
        );
    }
}

SeatScreen.navigationOptions = () => {
    return {
        headerTitle: 'Select Seat',
        headerTitleAlign: 'center',
        headerTitleStyle: {
            fontFamily: 'primary-medium',
            fontSize: 16,
        },
        headerStyle: {
            backgroundColor: theme.colors.white,
        },
        headerTintColor: theme.colors.tertiaryColor,
    };
};

export default SeatScreen;
