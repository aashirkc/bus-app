import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    ActivityIndicator,
    TouchableNativeFeedback,
    TouchableWithoutFeedback
} from 'react-native';
import { Ionicons, Entypo } from '@expo/vector-icons';
import {apiEndpoint} from '../../components/services/busService'
import { RoutesData } from '../../data/carousel-data';
import {getBusFromBusId} from '../../components/services/busService'
import * as theme from '../../constants/theme';
import UnderLine from '../../components/utils/underline';
import Articles from '../../components/carousel';
import { styles } from '../../assets/css/busDetailScreen';

class BusDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            BusInfo: [],
            errors: null,
            features: [],
            loading: true,
        };
    }

    componentDidMount() {
        try {
            const busId = this.props.navigation.getParam('busId');
            console.log("route",busId)
            getBusFromBusId(busId).then(data=>{
                if(data){
                    this.setState({
                        BusInfo: data.payload, 
                        features:JSON.parse(data.payload.features) , 
                        loading: false
                    })  
                    
                }
            })
            
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    render() {
        const { BusInfo, features, loading, errors } = this.state;
        return (
            <>
                {errors ? (
                    <View>
                        <Text>Errors : {errors}</Text>
                    </View>
                ) : loading ? (
                    <ActivityIndicator
                        size="large"
                        color={theme.colors.tertiaryColor}
                    />
                ) : (
                  <TouchableWithoutFeedback>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.screen}>
                            <View style={styles.busHeader}>
                                <View style={styles.HeaderTop}>
                                    <View style={styles.headerback}>
                                        <TouchableNativeFeedback
                                            onPress={() =>
                                                this.props.navigation.goBack()
                                            }
                                        >
                                            <Ionicons
                                                name="md-arrow-back"
                                                size={22}
                                                color="white"
                                                style={styles.backIcon}
                                            />
                                        </TouchableNativeFeedback>
                                    </View>
                                    <View style={styles.headerTitle}>
                                        <Text style={styles.headerTitleText}>
                                            Bus Details
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.headerMain}>
                                    <View>
                                        <Text style={styles.headerTopText}>
                                            Departure
                                        </Text>
                                        <Text
                                            style={styles.headermiddleText}
                                            numberOfLines={2}
                                        >
                                            {BusInfo.startPlace}
                                        </Text>
                                        <Text style={styles.headerbottomText}>
                                            {BusInfo.departureDay}
                                        </Text>
                                    </View>
                                    <View>
                                        <Ionicons
                                            name="md-arrow-forward"
                                            size={30}
                                            color="white"
                                            style={styles.iconMain}
                                        />
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.headerTopText}>
                                            Arrive
                                        </Text>
                                        <Text
                                            style={styles.headermiddleText}
                                            numberOfLines={2}
                                        >
                                            {BusInfo.destinationPlace}
                                        </Text>
                                        <Text style={styles.headerbottomText}>
                                            {BusInfo.arrivalDay}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.busDate}>
                                <View style={styles.busDateInfo}>
                                    <View>
                                        <Text style={styles.dateInfo}>
                                            {BusInfo.departureDay}
                                        </Text>
                                    </View>
                                    <View>
                                        <Ionicons
                                            name="md-calendar"
                                            size={30}
                                            color={theme.colors.lightgray}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.busDetailsWrapper}>
                                <View style={styles.busdetailsWrap}>
                                    <View>
                                        <Text style={styles.busDetailTitle}>
                                            Type
                                        </Text>
                                        <Text style={styles.busDetailInfo}>
                                            {BusInfo.type}
                                        </Text>
                                    </View>
                                    {/* <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.busDetailTitle}>
                                            Discount
                                        </Text>
                                        <Text style={styles.busDetailInfo}>
                                            {BusInfo.basicInfo.discount}
                                        </Text>
                                    </View> */}
                                </View>
                                <View style={styles.busdetailsWrap}>
                                    {/* <View>
                                        <Text style={styles.busDetailTitle}>
                                            Duration
                                        </Text>
                                        <Text style={styles.busDetailInfo}>
                                            {BusInfo.routeName.journeyTime}
                                        </Text>
                                    </View> */}
                                    {/* <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.busDetailTitle}>
                                            Charge
                                        </Text>
                                        <Text style={styles.busDetailInfo}>
                                            NRs. {BusInfo.basicInfo.charge}
                                        </Text>
                                    </View> */}
                                </View>
                            </View>
                            <View style={styles.buttonContainer}>
                                <TouchableNativeFeedback
                                    background={TouchableNativeFeedback.Ripple(
                                        '#000000',
                                        true
                                    )}
                                    onPress={() =>
                                        this.props.navigation.navigate('Seats')
                                    }
                                >
                                    <Text style={styles.button}>Book Now</Text>
                                </TouchableNativeFeedback>
                            </View>
                            <UnderLine />
                            <View style={styles.BusInfo}>
                                <Text style={styles.busInfoText}>
                                    Bus Details :
                                </Text>
                                <View style={styles.busInfoWrapper}>
                                    <View style={styles.busInfoWrap}>
                                        <Text style={styles.businfodetailText}>
                                            Bus Number :{' '}
                                        </Text>
                                        <Text
                                            style={styles.businfodetailTextMain}
                                        >
                                            {BusInfo.busNumber}
                                        </Text>
                                    </View>
                                    <View style={styles.busInfoWrap}>
                                        <Text style={styles.businfodetailText}>
                                            Name :{' '}
                                        </Text>
                                        <Text
                                            style={styles.businfodetailTextMain}
                                        >
                                            {BusInfo.busName}
                                        </Text>
                                    </View>
                                    <View style={styles.busInfoWrap}>
                                        <Text style={styles.businfodetailText}>
                                            Driver Name :{' '}
                                        </Text>
                                        <Text
                                            style={styles.businfodetailTextMain}
                                        >
                                            {BusInfo.driverName}
                                        </Text>
                                    </View>
                                    <View style={styles.busInfoWrap}>
                                        <Text style={styles.businfodetailText}>
                                            Driver Number :{' '}
                                        </Text>
                                        <Text
                                            style={styles.businfodetailTextMain}
                                        >
                                            {BusInfo.driverNumber}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <UnderLine />
                            <View style={styles.BusInfo}>
                                <Text
                                    style={{
                                        ...styles.busInfoText,
                                        width: 120,
                                    }}
                                >
                                    Bus Features :
                                </Text>
                                <View style={styles.busInfoWrapper}>
                                    {features.map((m, index) => (
                                        <View
                                            style={styles.busInfoWrap}
                                            key={index}
                                        >
                                            <Text
                                                style={
                                                    styles.businfodetailTextMain
                                                }
                                            >
                                                <Entypo
                                                    name="dot-single"
                                                    size={20}
                                                />{' '}
                                                {m.label}
                                            </Text>
                                        </View>
                                    ))}
                                </View>
                            </View>
                            <UnderLine />
                            <View style={styles.carouselImage}>
                                <View style={styles.BusInfo}>
                                    <Text
                                        style={{
                                            ...styles.busInfoText,
                                            width: 110,
                                        }}
                                    >
                                        Bus Images :
                                    </Text>
                                </View>
                                <Image 
                                style={styles.logo}
                               source={{uri: `${apiEndpoint}/photo/${BusInfo._id}`}}/>
                            </View>
                        </View>
                    </ScrollView>
                </TouchableWithoutFeedback>
                )}
            </>
        );
    }
}

export default BusDetails;
