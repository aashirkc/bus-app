import React, { Component } from 'react';
import { View,  StyleSheet, ScrollView, TouchableWithoutFeedback } from 'react-native';

import BusCard from '../../components/utils/BusCard';
import * as theme from '../../constants/theme';
import {getBusFromRouteId, getBus} from '../../components/services/busService'
// import {getRoutes} from './services/routeService'
import HeaderTitle from '../../components/utils/HeadeTitle';

class BusLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            BusList: [],
            errors: {},
        };
    }

    async componentDidMount() {    
        try {
            const routeId = this.props.navigation.getParam('busId');
            if(routeId){
                getBusFromRouteId(routeId).then(data=>{
                    
                    if(data){
                        this.setState({BusList: data.payload, loading: false})
                    }
                })
            }else{
                 getBus().then(data=>{
                    
                    if(data){
                        this.setState({BusList: data.payload, loading: false})
                    }
                })
            }
        } catch (err) {
            this.setState({ errors: err });
        }
    }

      navigationMain = (name, id) => {
        if (id) {
            this.props.navigation.navigate({
                routeName: name,
                params: { busId: id },
            });
        }
        this.props.navigation.navigate({
            routeName: name,
        });
    };

    render() {
        const { BusList } = this.state;
        const { onClicked } = this.props;
        return (
            <TouchableWithoutFeedback>
                <ScrollView
                            showsVerticalScrollIndicator={false}
                            style={styles.screen}
                            scrollEventThrottle={16}
                        >
                        <View style={{ backgroundColor: theme.colors.backgroundGray }}>
                            <HeaderTitle title="Available Vehicle " />
                            {BusList &&
                                BusList.map(item => (
                                    <View style={styles.list} key={item._id}>
                                        <BusCard
                                            onSelectBus={() => this.navigationMain('BusInfo', item._id)}
                                            data = {item}
                                            start={item.startPlace}
                                            startTime={item.departureDay}
                                            destinationName={item.destinationPlace}
                                            destinationTime={item.arrivalDay}
                                            number={item.busNumber}
                                        />
                                    </View>
                                ))}
                        </View>
                </ScrollView>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
     screen: {
        flex: 1,
        backgroundColor: theme.colors.gray2,
        marginTop: 48,
        // backgroundColor: theme.colors.lightgray,
    },
    Home: {
        marginHorizontal: 5,
    },
    list: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 12,
        // marginVertical: 5,
    },
});




export default BusLists;
