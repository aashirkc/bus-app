import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import DateTimePicker from '@react-native-community/datetimepicker';
import {getDistrict, getRouteFromSearch} from '../components/services/routeService'
import DropDownPicker from 'react-native-dropdown-picker';
import * as theme from '../constants/theme';

class SearchPage extends Component {
    fieldRef = React.createRef();
    constructor(props) {
        super(props);

        this.state = {
            data: {
                From: '',
                To: '',
                dateTime: new Date(),
            },
            district: [],
            dateTimePickerVisible: false,
            dataResponse: false,
            error: false
        };
    }

    componentDidMount() {
          try {
                getDistrict().then(data=>{
                    if(data){
                        data.payload.features.map((items, i)=>{
                            let a = {
                                label: items.properties.DISTRICT,
                                value: items.properties.DISTRICT
                            }
                            this.state.district.push(a)
                        })

                        // this.setState({district: data.payload.features, loading: false})
                    }
                })
            
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.dataResponse !== prevState.dataResponse) {
            this.fieldRef.current.setValue(
                `${this.state.data.dateTime}`.substring(0, 15)
            );
        }
    }

    getValue = value => {
        if (value === undefined || value === null || value === '') {
            let values = new Date();
            return values;
        } else {
            return value;
        }
    };

      navigationMain = (name, id) => {
        if (id) {
            this.props.navigation.navigate({
                routeName: name,
                params: { busId: id },
            });
        }
        this.props.navigation.navigate({
            routeName: name,
        });
    };


    handleSubmit = () => {
        const { data } = this.state;
        getRouteFromSearch(data.From, data.To).then(data=>{
            if(data && data.payload){
                this.navigationMain('BusList', data.payload._id)
            }else{
                 this.setState({error: true})
                setTimeout(()=>{
                    this.setState({error: false})
                    
                }, 6000)
            }
        })
    };

     

    render() {
        const { dateTimePickerVisible, data, dataResponse, error } = this.state;
        return (
            <ScrollView>
                <View style={styles.search}>
                    <View style={styles.form}>
                        <View style={styles.formControl}>
                           
                            <Text>
                                Location From
                            </Text>

                                 <DropDownPicker
                                        items={this.state.district}
                                        // defaultValue={this.state.country}
                                        containerStyle={{height: 40}}
                                        style={{backgroundColor: '#fafafa', marginBottom:5}}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        dropDownStyle={{backgroundColor: '#fafafa'}}
                                        onChangeItem={item => this.setState({
                                              data: { ...data, From: item.value },
                                        })}
                                    />

                        </View>
                        <View style={styles.formControl}>
                                <Text>
                                    Location To
                                </Text>

                                 <DropDownPicker
                                        items={this.state.district}
                                        // defaultValue={this.state.country}
                                        containerStyle={{height: 40}}
                                        style={{backgroundColor: '#fafafa'}}
                                        itemStyle={{
                                            justifyContent: 'flex-start'
                                        }}
                                        dropDownStyle={{backgroundColor: '#fafafa'}}
                                        onChangeItem={item => this.setState({
                                              data: { ...data, To: item.value },
                                        })}
                                    />
                        </View>
                        <View style={styles.formControl}>
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({
                                        dateTimePickerVisible: true,
                                    })
                                }
                            >
                                <TextField
                                    label="Select Date"
                                    ref={this.fieldRef}
                                    keyboardType="default"
                                    value={`${data.dateTime}`.substring(0, 15)}
                                    labelTextStyle={styles.label}
                                    textColor={theme.colors.titleColor}
                                    editable={false}
                                    inputContainerStyle={styles.input}
                                    containerStyle={styles.inputContainer}
                                />
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                ...styles.formControl,
                                ...styles.buttonContainer,
                            }}
                        >
                            <TouchableOpacity
                                onPress={this.handleSubmit}
                                setOpacityTo={0.6}
                            >
                                <Text style={styles.button}>Search</Text>
                            </TouchableOpacity>
                        </View>
                        {dateTimePickerVisible && (
                            <DateTimePicker
                                value={data.dateTime}
                                mode={'datetime'}
                                display="default"
                                onChange={(event, value) => {
                                    this.setState({
                                        data: {
                                            ...data,
                                            dateTime: this.getValue(value),
                                        },
                                        dataResponse: !dataResponse,
                                        dateTimePickerVisible:
                                            Platform.OS === 'ios'
                                                ? true
                                                : false,
                                    });
                                }}
                            />
                        )}
                        { error === true && (
                            <View>
                                <Text style={{color: "red", textAlign: "center"}}>
                                    Sorry !! No Bus Available in this route
                                </Text>
                            </View>
                        )}
                    </View>
                      
                </View>
             
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    search: {
        backgroundColor: theme.colors.white,
        flex: 1,
    },
    form: {
        marginTop: 170,
        margin: 20,
    },
    formControl: {
        width: '100%',
        marginTop:20
    },
    label: {
        fontFamily: 'primary-regular',
        color: theme.colors.primaryColor,
    },
    input: {
        borderRadius: 10,
    },
    inputContainer: {
        marginVertical: 0,
    },
    placeholders: {
        fontFamily: 'primary-medium',
    },
    buttonContainer: {
        marginVertical: 15,
    },
    button: {
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        backgroundColor: theme.colors.tertiaryColor,
        color: theme.colors.white,
        borderRadius: 2,
        fontFamily: 'primary-semibold',
    },
});

export default SearchPage;
