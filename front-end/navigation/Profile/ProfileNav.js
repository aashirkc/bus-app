import { createStackNavigator } from 'react-navigation-stack';
import NavHead from '../utils/navHead';

import ProfileScreen from '../../screens/ProfileScreen';

const ProfileNavigator = createStackNavigator({
    Profile: NavHead(ProfileScreen),
});

export default ProfileNavigator;
