import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import HeaderImage from '../../components/utils/HeaderImage';
import * as theme from '../../constants/theme';

import BusSearchScreen from '../../screens/BusSearchScreen';

const SearchNavigator = createStackNavigator({
    Search: {
        screen: BusSearchScreen,
        navigationOptions: {
            headerBackground: () => (
                <HeaderImage
                    source={require('../../assets/images/bussin_drib.jpg')}
                />
            ),
            headerTitleStyle: {
                fontFamily: 'primary-semibold',
                fontSize: 17,
                color: theme.colors.white,
            },
            headerTitle: 'Search Bus Route',
        },
    },
});

export default SearchNavigator;
