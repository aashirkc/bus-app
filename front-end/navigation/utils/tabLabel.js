import React from 'react';
import { Text } from 'react-native';

const TabLebal = tabName => {
    return (
        <Text
            style={{
                fontSize: 10,
                fontFamily: 'primary-regular',
                textTransform: 'uppercase',
            }}
        >
            {tabName}
        </Text>
    );
};

export default TabLebal;
