import * as theme from '../../constants/theme';

const NavHead = Name => {
    return {
        screen: Name,
        navigationOptions: {
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: theme.colors.white,
            },
            headerTintColor: theme.colors.tertiaryColor,
            headerTitleStyle: {
                fontFamily: 'primary-medium',
                fontSize: 17,
            },
        },
    };
};

export default NavHead;
