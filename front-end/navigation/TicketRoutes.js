import React from 'react';

import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Ionicons, FontAwesome, Feather } from '@expo/vector-icons';

import SearchNavigator from './Search/SearchNav';
import BookedNavigator from './Booked/BookedNav';
import ProfileNavigator from './Profile/ProfileNav';
import HomeNavigator from './Home/Home';

import * as theme from '../constants/theme';
import TabLebal from './utils/tabLabel';

const TicketTabNavigator = createMaterialBottomTabNavigator(
    {
        Home: {
            screen: HomeNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <FontAwesome name="home" size={22} color={tintColor} />
                    );
                },
                tabBarLabel: TabLebal('Home'),
            },
        },
        Search: {
            screen: SearchNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Feather name="search" size={22} color={tintColor} />
                    );
                },
                tabBarLabel: TabLebal('Search'),
            },
        },
        Booking: {
            screen: BookedNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Ionicons
                            name="ios-bookmark"
                            size={22}
                            color={tintColor}
                        />
                    );
                },
                tabBarLabel: TabLebal('inbox'),
            },
        },
        Profile: {
            screen: ProfileNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Ionicons
                            name="md-settings"
                            size={25}
                            color={tintColor}
                        />
                    );
                },
                tabBarLabel: TabLebal('settings'),
            },
        },
    },
    {
        activeColor: theme.colors.white,
        inactiveColor: '#dfe0e0',
        barStyle: {
            backgroundColor: theme.colors.tertiaryColor,
            borderTopWidth: 0,
            shadowOffset: { width: 5, height: 3 },
            shadowColor: '#000',
            shadowOpacity: 0.5,
            elevation: 5,
        },
    }
);

export default createAppContainer(TicketTabNavigator);
