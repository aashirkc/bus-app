import { createStackNavigator } from 'react-navigation-stack';

import NavHead from '../utils/navHead';
import BookedScreen from '../../screens/BookedScreen';

const BookedNavigator = createStackNavigator({
    Booking: NavHead(BookedScreen),
});

export default BookedNavigator;
