import { createStackNavigator } from 'react-navigation-stack';

import NavHead from '../utils/navHead';

import HomeScreen from '../../screens/HomeScreen';

import LocationScreen from '../../screens/Home/LocationScreen';
import OfferScreen from '../../screens/Home/OfferScreen';
import RouteScreen from '../../screens/Home/RouteScreen';
import TimeScreen from '../../screens/Home/TimeScreen';
import BusScreen from '../../screens/Home/BusScreen';

import MapScreen from '../../screens/IconScreens/MapScreeen';
import BusDetails from '../../screens/busInfo/BusDetailScreen';
import BusLists from '../../screens/busInfo/BusListingScreen';
import SeatScreen from '../../screens/busInfo/BusSeatScreen';
import PaymentScreen from '../../screens/payment/paymentScreen';

const HomeNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: { headerShown: false },
    },
    BusScreen: NavHead(BusScreen),
    LocationScreen: NavHead(LocationScreen),
    OfferScreen: NavHead(OfferScreen),
    RouteScreen: NavHead(RouteScreen),
    TimeScreen: NavHead(TimeScreen),

    //Icon Routes
    MapScreen: NavHead(MapScreen),

    //Bus Details
    BusInfo: {
        screen: BusDetails,
        navigationOptions: {
            headerShown: false,
        },
    },

       //Bus Listing
    BusList: {
        screen: BusLists,
        navigationOptions: {
            headerShown: false,
        },
    },

    Seats: {
        screen: SeatScreen,
    },

    //Payment Details
    Payment: NavHead(PaymentScreen),
});

export default HomeNavigator;
