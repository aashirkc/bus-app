import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableNativeFeedback,
    TouchableOpacity,
} from 'react-native';

import { Ionicons, Feather, Entypo } from '@expo/vector-icons';
import * as theme from '../constants/theme';

class HomeIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={styles.IconWrapper}>
                <TouchableNativeFeedback
                    onPress={() => this.props.Clicked('BusList')}
                >
                    <View style={styles.IconWrap}>
                        <Ionicons
                            name="md-bus"
                            size={26}
                            color={theme.colors.lightBlue}
                            style={styles.Icons}
                        />
                        <Text style={styles.IconText}>Bus</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback
                    onPress={() => this.props.Clicked('Search')}
                >
                    <View style={styles.IconWrap}>
                        <Feather
                            name="map"
                            size={26}
                            color={theme.colors.lightBlue}
                            style={styles.Icons}
                        />
                        <Text style={styles.IconText}>Route</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback
                    onPress={() => this.props.Clicked('TimeScreen')}
                >
                    <View style={styles.IconWrap}>
                        <Ionicons
                            name="md-calendar"
                            size={26}
                            color={theme.colors.lightBlue}
                            style={styles.Icons}
                        />
                        <Text style={styles.IconText}>Calendar</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback
                    onPress={() => this.props.Clicked('MapScreen')}
                >
                    <View style={styles.IconWrap}>
                        <Entypo
                            name="location"
                            size={26}
                            color={theme.colors.lightBlue}
                            style={styles.Icons}
                        />
                        <Text style={styles.IconText}>Location</Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    IconWrapper: {
        flexDirection: 'row',
        marginVertical: 15,
        marginHorizontal: 10,
        borderBottomColor: theme.colors.gray,
        borderBottomWidth: 1,
        overflow: 'hidden',
    },
    IconWrap: {
        paddingVertical: 10,
        marginBottom: 15,
        borderRadius: 5,
        overflow: 'hidden',
        marginRight: '3.1%',
        width: '22.5%',
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',

        shadowColor: theme.colors.black,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 10,

        backgroundColor: theme.colors.white,
    },
    Icons: {
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    IconText: {
        color: '#575757',
        fontSize: 12,
        fontFamily: 'primary-medium',
    },
});

export default HomeIcon;
