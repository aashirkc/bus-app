import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Card from './utils/Card';
import * as theme from '../constants/theme';
import {getRoutes} from './services/routeService'
import { RoutesData } from '../data/carousel-data';
import HeaderTitle from '../components/utils/HeadeTitle';

class PopularRoutes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Routes: [],
            errors: {},
        };
    }

    async componentDidMount() {      
        try {
            getRoutes().then(data=>{ 
                    this.setState({ Routes: data.payload})
                    })
            // this.setState({ Routes: RoutesData });
        } catch (err) {
            this.setState({ errors: err });
        }
    }

    render() {
        const { Routes } = this.state;
        const { onClicked } = this.props;
        return (
            <View style={{ backgroundColor: theme.colors.backgroundGray }}>
                <HeaderTitle title="Popular Bus Routes" />
                {Routes &&
                    Routes.map(item => (
                        <View style={styles.list} key={item._id}>
                            <Card
                                onSelectRoutes={() => onClicked(item._id)}
                                start={item.startDistrict}
                                routeName={item.routeName}
                                destinationName={item.endDistrict}
                                
                            />
                        </View>
                    ))}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 12,
        // marginVertical: 5,
    },
});

export default PopularRoutes;
