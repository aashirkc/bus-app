import {Api} from './config'

export const apiEndpoint = `${Api}/api/bus`;


export function getBusFromRouteId(id) {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}/byroute/${id}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

export function getBusFromBusId(id) {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}/${id}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

export function getBus() {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}


export function getBusFromSearch(from, to) {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}/by/district?From=${from}&To=${to}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

