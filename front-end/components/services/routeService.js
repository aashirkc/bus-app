import {Api} from './config'

const apiEndpoint = `${Api}/api/routes`;



export function getRoutes() {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

export function getDistrict(){
	return fetch(`${apiEndpoint}/dis`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

export function getRouteFromSearch(from, to) {
	console.log(apiEndpoint)
	return fetch(`${apiEndpoint}/by/district?From=${from}&To=${to}`, {
		method: 'GET',
		headers:{
			Accept: 'application/json',
    		'Content-Type': 'application/json'
		}
	}).then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
}

