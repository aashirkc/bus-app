import React, { useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Button,
    TouchableNativeFeedback,
    Image,
} from 'react-native';

import {apiEndpoint} from '../services/busService'


import * as theme from '../../constants/theme';

const BusCard = props => {

    useEffect(()=>{
        console.log(`${apiEndpoint}/photo/${props.data._id}`)
    },[])
    return (
        
        <TouchableNativeFeedback
            background={TouchableNativeFeedback.Ripple('#ffffff6b', true)}
            onPress={props.onSelectBus}
        >
            <View style={styles.card}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardWrapper}>

                        <View style={styles.cardImage}>
                             <Text>
                                Bus Number: {props.number}
                            </Text>
                               <Image 
                                style={styles.logo}
                               source={{uri: `${apiEndpoint}/photo/${props.data._id}`}}/>
                        </View>
                        <View style={styles.startPlace}>
                            <Text>
                                Start Place: {props.start}
                            </Text>
                            <Text>
                                Destination place: {props.destinationName}
                            </Text>
                            
                        </View>
                         <View style={styles.Date}>
                            <Text>
                                Start Date: {props.startTime}
                            </Text>
                            <Text>
                                Destination Date: {props.destinationTime}
                            </Text>
                            <Button style={styles.button}
                                title="Click to Book"
                                onPress={props.onSelectBus}
                                />
                            
                        </View>

                      
                       
                    </View>
                </View>
            </View>
        </TouchableNativeFeedback>
    );
};

BusCard.navigationOptions = {
    headerTitle: 'Meal',
};

const styles = StyleSheet.create({
    card: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginBottom: 55,
        flexDirection: 'row',
        borderRadius: 4,
        borderBottomColor: theme.colors.white,
        height: "90%",
        shadowColor: theme.colors.black,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 10,

        backgroundColor: theme.colors.white,
    },
  
    logo: {
        width: "100%",
        height: "100%",
    },
    cardContainer: {
        height: 300,
        width: '100%',
    },
   
    cardImage: {
        height: '60%',
        width: '100%',
    },
    startPlace: {
        fontFamily: 'primary-medium !important',
        fontSize: 12,
        marginTop: 18,
        textTransform: 'uppercase',
        flexDirection: 'column',
        justifyContent: 'space-between',
        color: theme.colors.titleColor, 
    },
    button: {
        maxWidth: 10
    },
    cardTime: {
        fontFamily: 'primary-semibold',
        fontSize: 14,
        color: '#7599fd',
    },
    cardImagePlace: {
        fontSize: 11,
        fontFamily: 'primary-regular',
        color: theme.colors.lightgray,
    },
    cardImagePlace1: {
        marginTop: 7,
    },
});

export default BusCard;
