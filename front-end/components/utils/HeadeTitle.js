import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import * as theme from '../../constants/theme';

const HeaderTitle = props => {
    return (
        <View>
            <Text style={{ ...styles.title, ...props.style }}>
                {props.title}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    title: {
        marginHorizontal: 12,
        paddingBottom: 10,
        fontFamily: 'primary-medium',
        color: theme.colors.titleColor,
    },
});

export default HeaderTitle;
