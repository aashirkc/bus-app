import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const HeaderImage = props => {
    return (
        <View style={styles.screen}>
            <Image style={styles.image} source={props.source} />
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        width: '100%',
        height: 250,
    },
    image: {
        height: '100%',
        width: '100%',
    },
});

export default HeaderImage;
