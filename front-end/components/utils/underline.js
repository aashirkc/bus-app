import React from 'react';
import { View, StyleSheet } from 'react-native';

import * as theme from '../../constants/theme';

const UnderLine = () => {
    return <View style={styles.screen}></View>;
};

const styles = StyleSheet.create({
    screen: {
        borderBottomColor: theme.colors.gray,
        borderBottomWidth: 1.1,
        width: '100%',
    },
});

export default UnderLine;
