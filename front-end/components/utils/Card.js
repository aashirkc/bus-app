import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableNativeFeedback,
    Image,
} from 'react-native';

import * as theme from '../../constants/theme';

const Card = props => {
    return (
        <TouchableNativeFeedback
            background={TouchableNativeFeedback.Ripple('#ffffff6b', true)}
            onPress={props.onSelectRoutes}
        >
            <View style={styles.card}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardWrapper}>
                        <View>
                            <Text style={styles.cardPlace} numberOfLines={1}>
                                {props.start}
                            </Text>
                            
                        </View>
                        <View style={styles.cardImageWrapper}>
                             <Text style={styles.cardImagePlace}>
                                {props.routeName}
                            </Text>
                            <Text style={styles.cardImagePlace}>
                                {props.time}
                            </Text>
                            <Image
                                source={require('../../assets/images/buss.png')}
                                style={styles.cardImage}
                            />
                            <Text
                                style={{
                                    ...styles.cardImagePlace,
                                    ...styles.cardImagePlace1,
                                }}
                            >
                                {props.runwaymode}
                            </Text>
                        </View>
                        <View>
                            <View style={{ alignItems: 'flex-end' }}>
                                <Text
                                    style={{
                                        ...styles.cardPlace,
                                        textAlign: 'right',
                                    }}
                                    numberOfLines={1}
                                >
                                    {props.destinationName}
                                </Text>
                                
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableNativeFeedback>
    );
};

Card.navigationOptions = {
    headerTitle: 'Meal',
};

const styles = StyleSheet.create({
    card: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginBottom: 15,
        flexDirection: 'row',
        borderRadius: 2,

        shadowColor: theme.colors.black,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 10,

        backgroundColor: theme.colors.white,
    },
    cardContainer: {
        overflow: 'hidden',
        height: 60,
        width: '100%',
    },
    cardWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
    },
    cardImageWrapper: {
        height: 20,
        minWidth: 125,
        width: '40%',
        maxWidth: '50%',
        transform: [{ translateY: -15 }],
        alignItems: 'center',
    },
    cardImage: {
        height: '100%',
        width: '100%',
    },
    cardPlace: {
        fontSize: 12,
        fontFamily: 'primary-regular',

        color: theme.colors.titleColor,
        width: 80,
    },
    cardTime: {
        fontFamily: 'primary-semibold',
        fontSize: 14,
        color: '#7599fd',
    },
    cardImagePlace: {
        fontSize: 11,
        fontFamily: 'primary-regular',
        color: theme.colors.lightgray,
    },
    cardImagePlace1: {
        marginTop: 7,
    },
});

export default Card;
