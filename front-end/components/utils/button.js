import React from 'react';
import { View, Text, StyleSheet, TouchableNativeFeedback } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import * as theme from '../../constants/theme';

const Buttons = props => {
    const ComponentName = props.component;
    return (
        <TouchableNativeFeedback onPress={props.clickHandler}>
            <View style={styles.buttonContainer}>
                {ComponentName && (
                    <ComponentName
                        name={props.icon}
                        size={25}
                        color={'white'}
                        style={styles.Icons}
                    />
                )}
                <Text style={styles.buttonText}>{props.title}</Text>
            </View>
        </TouchableNativeFeedback>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        height: 40,
        width: 'auto',
        backgroundColor: theme.colors.tertiaryColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        alignItems: 'center',
        borderRadius: 5,
    },
    Icons: {
        paddingRight: 10,
    },
    buttonText: {
        color: theme.colors.white,
        fontFamily: 'primary-medium',
        fontSize: 13,
    },
});

export default Buttons;
