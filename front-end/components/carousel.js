import React, { Component } from 'react';
import {
    Animated,
    StyleSheet,
    View,
    ScrollView,
    FlatList,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
} from 'react-native';

import * as theme from '../constants/theme';

const { width } = Dimensions.get('window');
class Articles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {},
        };
    }

    scrollX = new Animated.Value(0);
    renderDots() {
        const { ImageData } = this.props;
        const dotPosition = Animated.divide(this.scrollX, width);
        return (
            <View
                style={[
                    styles.flex,
                    styles.row,
                    {
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 10,
                    },
                ]}
            >
                {ImageData.map((item, index) => {
                    const borderWidth = dotPosition.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 2.5, 0],
                        extrapolate: 'clamp',
                    });
                    return (
                        <Animated.View
                            key={`step-${item.id}`}
                            style={[
                                styles.dots,
                                styles.activeDot,
                                { borderWidth: borderWidth },
                            ]}
                        />
                    );
                })}
            </View>
        );
    }

    renderDestinations = data => {
        return (
            <View style={[styles.column, styles.destinations]}>
                <FlatList
                    horizontal
                    pagingEnabled
                    scrollEnabled
                    showsHorizontalScrollIndicator={false}
                    decelerationRate={0}
                    scrollEventThrottle={16}
                    snapToAlignment="center"
                    style={{ overflow: 'visible', height: 185 }}
                    data={data}
                    keyExtractor={(item, index) => `${item.id}`}
                    onScroll={Animated.event([
                        { nativeEvent: { contentOffset: { x: this.scrollX } } },
                    ])}
                    renderItem={({ item }) => this.renderDestination(item)}
                />
                {this.renderDots()}
            </View>
        );
    };

    renderDestination = item => {
        return (
            <TouchableOpacity activeOpacity={0.8} onPress={this.props.Clicked}>
                <ImageBackground
                    style={[styles.flex, styles.destination, styles.shadow]}
                    imageStyle={{ borderRadius: theme.sizes.radius }}
                    source={{ uri: item.name }}
                ></ImageBackground>
            </TouchableOpacity>
        );
    };

    render() {
        const { ImageData } = this.props;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 5 }}
            >
                {this.renderDestinations(ImageData)}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    flex: {
        flex: 0,
    },
    column: {
        flexDirection: 'column',
    },
    row: {
        flexDirection: 'row',
    },
    destinations: {
        flex: 1,
        justifyContent: 'space-between',
    },
    destination: {
        width: width - 30,
        height: '100%',
        marginHorizontal: 10,
        borderRadius: theme.sizes.radius,
    },
    shadow: {
        shadowColor: theme.colors.black,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.05,
        shadowRadius: 10,
        elevation: 5,
    },
    dots: {
        width: 8,
        height: 8,
        borderWidth: 2,
        borderRadius: 5,
        marginHorizontal: 6,
        backgroundColor: theme.colors.gray,
        borderColor: 'transparent',
    },
    activeDot: {
        width: 10.5,
        height: 10.5,
        borderRadius: 6.25,
        borderColor: theme.colors.lightBlue,
        // backgroundColor: theme.colors.active,
    },
});

export default Articles;
