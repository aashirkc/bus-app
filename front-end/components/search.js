import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Platform, StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as Animatable from 'react-native-animatable';
import * as theme from '../constants/theme';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchFocused: this.props.searchFocused,
        };
    }

    render() {
        let startHeaderheight = 40;
        if (Platform.OS === 'android') {
            startHeaderheight = 35 + StatusBar.currentHeight;
        }

        return (
            <View style={styles.searchContainer} height={startHeaderheight}>
                <Animatable.View style={styles.searchWrapper}>
                    <TextInput
                        placeholder="Search...."
                        underlineColorAndroid="transparent"
                        placeholderTextColor="grey"
                        style={styles.searchInput}
                    />
                    <Animatable.View
                        animation={
                            this.state.searchbarFocused
                                ? 'fadeInLeft'
                                : 'fadeInRight'
                        }
                    >
                        <Ionicons
                            name="ios-search"
                            size={23}
                            style={styles.searchIcon}
                        />
                    </Animatable.View>
                </Animatable.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    searchContainer: {
        marginTop: 5,
        marginHorizontal: 5,
    },
    searchWrapper: {
        flexDirection: 'row',
        paddingRight: 10,
        paddingLeft: 20,
        backgroundColor: 'rgba(0,0,0,0.1)',

        borderRadius: 50,
        marginTop: 8,
    },
    searchIcon: {
        justifyContent: 'flex-end',
        padding: 5,
        paddingLeft: 10,
        color: theme.colors.lightBlue,
    },
    searchInput: {
        flex: 1,
        fontWeight: '400',
    },
});

export default Search;
