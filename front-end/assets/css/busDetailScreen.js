import { StyleSheet } from 'react-native';
import * as theme from '../../constants/theme';

export const styles = StyleSheet.create({
    screen: {
        marginTop: 35,
        backgroundColor: theme.colors.white,
    },
    busHeader: {
        paddingVertical: 20,
        paddingHorizontal: 20,
        height: 250,
        width: '100%',
        backgroundColor: theme.colors.tertiaryColor,
    },
    HeaderTop: {
        flexDirection: 'row',
    },
    headerback: {
        justifyContent: 'flex-start',
        backgroundColor: 'transparent',
    },
    headerTitle: {
        flex: 1,
    },
    headerTitleText: {
        textAlign: 'center',
        color: theme.colors.white,
        fontFamily: 'primary-medium',
        fontSize: 16,
    },
    headerMain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 40,
    },
    headerTopText: {
        paddingBottom: 15,
        fontFamily: 'primary-semibold',
        color: theme.colors.white,
    },
    logo: {
        height:"100%",
        width: "100%"
    },
    headermiddleText: {
        maxWidth: 160,
        fontSize: 23,
        color: '#fdf3d5',
        fontFamily: 'primary-bold',
        textAlign: 'right',
    },
    iconMain: {
        transform: [{ translateY: 35 }],
    },
    headerbottomText: {
        paddingTop: 15,
        fontFamily: 'primary-medium',
        color: theme.colors.white,
        fontSize: 16,
        textTransform: 'uppercase',
    },
    busDate: {
        alignItems: 'center',
        transform: [{ translateY: -25 }],
    },
    busDateInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
        height: 50,
        width: '85%',
        backgroundColor: theme.colors.white,
        borderRadius: 10,

        shadowColor: theme.colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 4,
    },
    dateInfo: {
        fontFamily: 'primary-medium',
    },
    busDetailsWrapper: {
        paddingHorizontal: 40,
        paddingBottom: 10,
    },
    busdetailsWrap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 20,
    },
    busDetailTitle: {
        paddingBottom: 5,
        fontFamily: 'primary-medium',
        color: theme.colors.black,
        fontSize: 12,
    },
    busDetailInfo: {
        fontFamily: 'primary-semibold',
        color: theme.colors.titleColor,
        fontSize: 16,
        maxWidth: 150,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    buttonContainer: {
        marginHorizontal: 20,
        marginBottom: 15,
    },
    button: {
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        backgroundColor: theme.colors.tertiaryColor,
        color: theme.colors.white,
        borderRadius: 2,
        fontFamily: 'primary-semibold',
    },
    BusInfo: {
        paddingHorizontal: 20,
        paddingVertical: 15,
        justifyContent: 'center',
    },
    busInfoText: {
        fontFamily: 'primary-semibold',
        color: theme.colors.black,
        fontSize: 16,
        borderBottomColor: theme.colors.tertiaryColor,
        borderBottomWidth: 2,
        paddingBottom: 5,
        width: 102,
    },
    busInfoWrapper: {
        paddingTop: 15,
    },
    busInfoWrap: {
        flexDirection: 'row',
        paddingBottom: 10,
    },
    businfodetailText: {
        fontFamily: 'primary-semibold',
        color: theme.colors.titleColor,
    },
    businfodetailTextMain: {
        fontFamily: 'primary-medium',
        color: theme.colors.black,
        fontSize: 14,
    },
    carouselImage: {
        marginHorizontal: 5,
        height: 100,
        // width: 30,
        marginBottom: 10,
    },
    backIcon: {
        height: 20,
        width: 30,
        backgroundColor: 'transparent',
    },
});
