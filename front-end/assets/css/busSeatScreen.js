import { StyleSheet } from 'react-native';
import * as theme from '../../constants/theme';

export const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    seatInfo: {
        marginTop: 20,
        marginHorizontal: 10,
        backgroundColor: theme.colors.white,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    seatMain: {
        marginTop: 20,
        marginHorizontal: 10,
        backgroundColor: theme.colors.white,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
        flex: 1,
    },
    seatInfoWrapper: {
        alignItems: 'center',
    },
    seatInfoBox: {
        height: 40,
        width: 40,
        backgroundColor: theme.colors.gray,
        borderRadius: 10,
        marginBottom: 5,
    },
    seatInfoText: {
        fontSize: 12,
        color: theme.colors.lightgray,
        fontFamily: 'primary-medium',
    },
    seatImageWrapper: {
        height: 40,
        width: 40,
    },
    seatMainWrapper: {
        paddingBottom: 10,
        paddingRight: 5,
        alignItems: 'flex-end',
    },
    seatImage: {
        height: '100%',
        width: '100%',
    },
    busSeatWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    busSeatWrapperlast: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-evenly',
    },
    seatStart: {
        width: 150,
    },
    seatSide: {
        flexDirection: 'row',
        marginBottom: 10,
        width: '100%',
    },
    seatBox: {
        height: 37,
        width: 37,
        backgroundColor: theme.colors.empty,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginHorizontal: 6,
        marginVertical: 2,
    },
    buttonText: {
        fontFamily: 'primary-regular',
        color: theme.colors.white,
    },
    selectedSeat: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
    },
    selectedSeatText: {
        fontFamily: 'primary-semibold',
        color: '#505050',
        fontSize: 12.5,
        paddingTop: 5,
    },
});
