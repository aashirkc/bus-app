import React, { useState } from 'react';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import TicketRoutes from './navigation/TicketRoutes';

const fetchFonts = () => {
    return Font.loadAsync({
        'primary-regular': require('./assets/fonts/Montserrat-Regular.ttf'),
        'primary-medium': require('./assets/fonts/Montserrat-Medium.ttf'),
        'primary-semibold': require('./assets/fonts/Montserrat-SemiBold.ttf'),
        'primary-bold': require('./assets/fonts/Montserrat-Bold.ttf'),
    });
};

export default function App() {
    const [dataLoaded, setDataLoaded] = useState(false);

    if (!dataLoaded) {
        return (
            <AppLoading
                startAsync={fetchFonts}
                onFinish={() => setDataLoaded(true)}
                onError={err => console.log(err)}
            />
        );
    }

    return <TicketRoutes />;
}
