const colors = {
    black: '#000',
    white: '#FFF',
    gray: '#DCE0E9',
    caption: '#BCCCD4',
    primaryColor: '#54d3c2',
    secondaryColor: '#5a60ee',
    tertiaryColor: '#3389fe',
    lightBlue: '#579cfb',
    lightgray: '#969696',
    titleColor: '#666666',
    backgroundGray: '#efefef',

    empty: '#eaeaea',
    selected: '#32e278',
    booked: '#2962d3',
};
//
const sizes = {
    base: 16,
    font: 14,
    padding: 36,
    margin: 36,
    title: 24,
    radius: 12,
};

export { colors, sizes };
