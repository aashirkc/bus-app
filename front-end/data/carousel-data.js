export const RoutesData = [
    {
        id: 1,
        routename: 'Mahendra Highway',
        start: 'Mechinagar',
        startName: 'M-NGR',
        startTime: '05:00 AM',
        destination: 'Itahari',
        destinationName: 'IT',
        destinationTime: '01:30 PM',
        leaverDay: 'Friday, 29 Mar 2020',
        time: '1hr 05min',
        distance: '108 kilometres',
        runwaymode: 'Non-Stop',
        basicInfo: {
            type: 'One Way',
            discount: 'Student',
            duration: '1hr 05min',
            charge: '500',
        },
        busDetail: {
            number: 'BA 1 KHA 1413',
            name: 'साझा यातायात',
            driver: 'Basanta Pandu',
            cName: 'Basanta Pandu',
        },
        features: [
            'Wi - Fi',
            'A/C and Fan System',
            'Comfortable Seats',
            'First Aid Kits',
            'Mobile Charger',
            'Mineral Water',
            'Experienced and Friendly Staffs',
            'On Time Departure',
        ],
        images: [
            {
                id: 4,
                name:
                    'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
            },
            {
                id: 1,
                name:
                    'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
            },
            {
                id: 2,
                name:
                    'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
            },
            {
                id: 3,
                name:
                    'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
            },
        ],
    },
    {
        id: 2,
        routename: 'Mahendra Highway',
        start: 'Hetauda',
        startName: 'HT',
        startTime: '05:00 AM',
        destination: 'Bhim Datta',
        destinationName: 'BD',
        destinationTime: '01:30 PM',
        time: '55min',
        distance: '80 kilometres',
        runwaymode: '1 Stop',
        leaverDay: 'Friday, 29 Mar 2020',
        basicInfo: {
            type: 'Two Way',
            discount: 'None',
            duration: '1hr 05min',
            charge: '500',
        },
        busDetail: {
            number: 'BA 1 KHA 1413',
            name: 'साझा यातायात',
            driver: 'Basanta Pandu',
            cName: 'Basanta Pandu',
        },
        features: [
            'Wi - Fi',
            'A/C and Fan System',
            'Comfortable Seats',
            'First Aid Kits',
            'Mobile Charger',
            'Mineral Water',
            'Experienced and Friendly Staffs',
            'On Time Departure',
        ],
        images: [
            {
                id: 4,
                name:
                    'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
            },
            {
                id: 1,
                name:
                    'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
            },
            {
                id: 2,
                name:
                    'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
            },
            {
                id: 3,
                name:
                    'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
            },
        ],
    },
    {
        id: 3,
        routename: 'Mahendra Highway',
        start: 'Kathmandu',
        startName: 'KTM',
        startTime: '05:00 AM',
        destination: 'Pokhara',
        destinationName: 'PHK',
        destinationTime: '01:30 PM',
        time: '1hr 35min',
        distance: '128 kilometres',
        runwaymode: 'Non-Stop',
        leaverDay: 'Friday, 29 Mar 2020',
        basicInfo: {
            type: 'One Way',
            discount: 'Student',
            duration: '1hr 05min',
            charge: '500',
        },
        busDetail: {
            number: 'BA 1 KHA 1413',
            name: 'साझा यातायात',
            driver: 'Basanta Pandu',
            cName: 'Basanta Pandu',
        },
        features: [
            'Wi - Fi',
            'A/C and Fan System',
            'Comfortable Seats',
            'First Aid Kits',
            'Mobile Charger',
            'Mineral Water',
            'Experienced and Friendly Staffs',
            'On Time Departure',
        ],
        images: [
            {
                id: 4,
                name:
                    'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
            },
            {
                id: 1,
                name:
                    'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
            },
            {
                id: 2,
                name:
                    'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
            },
            {
                id: 3,
                name:
                    'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
            },
        ],
    },
    {
        id: 4,
        routename: 'Mahendra Highway',
        start: 'Dharan',
        startName: 'DHRN',
        startTime: '05:00 AM',
        destination: 'Itahari',
        destinationName: 'IT',
        destinationTime: '01:30 PM',
        time: '1hr 05min',
        distance: '108 kilometres',
        runwaymode: '2 Stop',
        leaverDay: 'Friday, 29 Mar 2020',
        basicInfo: {
            type: 'One Way',
            discount: 'Student',
            duration: '1hr 05min',
            charge: '500',
        },
        busDetail: {
            number: 'BA 1 KHA 1413',
            name: 'साझा यातायात',
            driver: 'Basanta Pandu',
            cName: 'Basanta Pandu',
        },
        features: [
            'Wi - Fi',
            'A/C and Fan System',
            'Comfortable Seats',
            'First Aid Kits',
            'Mobile Charger',
            'Mineral Water',
            'Experienced and Friendly Staffs',
            'On Time Departure',
        ],
        images: [
            {
                id: 4,
                name:
                    'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
            },
            {
                id: 1,
                name:
                    'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
            },
            {
                id: 2,
                name:
                    'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
            },
            {
                id: 3,
                name:
                    'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
            },
        ],
    },
    {
        id: 5,
        routename: 'Mahendra Highway',
        start: 'Nepalgunj',
        startName: 'NPGJ',
        startTime: '05:00 AM',
        destination: 'Itahari',
        destinationName: 'HT',
        destinationTime: '01:30 PM',
        time: '1hr 25min',
        distance: '108 kilometres',
        runwaymode: 'Non-Stop',
        leaverDay: 'Friday, 29 Mar 2020',
        basicInfo: {
            type: 'One Way',
            discount: 'Student',
            duration: '1hr 05min',
            charge: '500',
        },
        busDetail: {
            number: 'BA 1 KHA 1413',
            name: 'साझा यातायात',
            driver: 'Basanta Pandu',
            cName: 'Basanta Pandu',
        },
        features: [
            'Wi - Fi',
            'A/C and Fan System',
            'Comfortable Seats',
            'First Aid Kits',
            'Mobile Charger',
            'Mineral Water',
            'Experienced and Friendly Staffs',
            'On Time Departure',
        ],
        images: [
            {
                id: 4,
                name:
                    'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
            },
            {
                id: 1,
                name:
                    'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
            },
            {
                id: 2,
                name:
                    'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
            },
            {
                id: 3,
                name:
                    'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
            },
        ],
    },
];

export const Images = [
    {
        id: 1,
        name:
            'https://images.unsplash.com/photo-1546955870-9fc9e5534349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    },
    {
        id: 2,
        name:
            'https://images.unsplash.com/photo-1501393091915-82f0cbd8f338?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
    },
    {
        id: 3,
        name:
            'https://images.unsplash.com/photo-1494515843206-f3117d3f51b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1504&q=80',
    },
    {
        id: 4,
        name:
            'https://images.unsplash.com/photo-1544620347-c4fd4a3d5957?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=749&q=80',
    },
];

export const BusSeat = {
    col1: [
        { name: 'A1', isbooked: false, isAvailable: false },
        { name: 'A2', isbooked: false, isAvailable: false },
        { name: 'A3', isbooked: false, isAvailable: false },
        { name: 'A4', isbooked: true, isAvailable: false },
        { name: 'A5', isbooked: false, isAvailable: false },
        { name: 'A6', isbooked: false, isAvailable: false },
        { name: 'A7', isbooked: false, isAvailable: false },
        { name: 'A8', isbooked: false, isAvailable: false },
        { name: 'A9', isbooked: false, isAvailable: false },
        { name: 'A10', isbooked: false, isAvailable: false },
    ],
    col2: [
        { name: 'B1', isbooked: false, isAvailable: false },
        { name: 'B2', isbooked: false, isAvailable: false },
        { name: 'B3', isbooked: false, isAvailable: false },
        { name: 'B4', isbooked: false, isAvailable: false },
        { name: 'B5', isbooked: false, isAvailable: false },
        { name: 'B6', isbooked: false, isAvailable: false },
        { name: 'B7', isbooked: true, isAvailable: false },
        { name: 'B8', isbooked: true, isAvailable: false },
        { name: 'B9', isbooked: false, isAvailable: false },
        { name: 'B10', isbooked: false, isAvailable: false },
    ],
    col3: [
        { name: 'C1', isbooked: true, isAvailable: false },
        { name: 'C2', isbooked: true, isAvailable: false },
        { name: 'C3', isbooked: false, isAvailable: false },
        { name: 'C4', isbooked: false, isAvailable: false },
        { name: 'C5', isbooked: false, isAvailable: false },
        { name: 'C6', isbooked: false, isAvailable: false },
        { name: 'C7', isbooked: false, isAvailable: false },
    ],
};
