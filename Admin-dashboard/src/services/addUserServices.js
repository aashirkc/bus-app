import http from './httpService';

const apiEndpoint = '/api/user';

export function addUser(value) {
	return http.post(`${apiEndpoint}`, value);
}

export function getUsers() {
	return http.get(`${apiEndpoint}`);
}

export function getUser(id) {
	return http.get(`${apiEndpoint}/${id}`);
}

export function editUser(id, value) {
	return http.put(`${apiEndpoint}/${id}`, value);
}

export function deleteUser(id) {
	return http.delete(`${apiEndpoint}/${id}`);
}
