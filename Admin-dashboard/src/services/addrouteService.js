import http from './httpService';

const apiEndpoint = '/api/routes';

export function addRoute(value) {
	return http.post(`${apiEndpoint}`, value);
}

export function getRoutes() {
	return http.get(`${apiEndpoint}`);
}


export function getDistrict() {
	return http.get(`${apiEndpoint}/dis`);
}


export function getRoute(id) {
	return http.get(`${apiEndpoint}/${id}`);
}

export function editRoute(id, value) {
	return http.put(`${apiEndpoint}/${id}`, value);
}

export function deleteRoute(id) {
	return http.delete(`${apiEndpoint}/${id}`);
}
