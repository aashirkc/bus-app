const SecureLS = require('secure-ls');

var ls = new SecureLS({
	encodingType: 'aes',
	encryptionSecret: '#$%$#^$&%589ru34ior[qo39',
});

export const setLocalStorage = (key, value) => (process.env.NODE_ENV === 'development' ? localStorage.setItem(key, value) : ls.set(key, value));
export const getLocalStorage = key => (process.env.NODE_ENV === 'development' ? localStorage.getItem(key) : ls.get(key) || null);
