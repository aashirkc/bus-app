import React from 'react';
import { Badge, Button } from 'reactstrap';
import { getDocUrl } from '../../services/httpService';
import { Link } from 'react-router-dom';
import { getDocUrl } from '../../services/httpService';
import Popup from "reactjs-popup";

const getBadge = status => {
  return status === 'true' ? 'success' : 'warning';
};

const activeClass = val => {
	let newClass = val === true ? 'badge-success' : 'badge-warning';
	return newClass;
};



export const getBusData = (data, handleDelete) => {
	console.log("dadsfs",data)
	const formData = data.map(row => {
		return {
			image: (
				<div className="avatar">
					<img src={`${getDocUrl()}/api/bus/photo/${row._id}`} alt="admin" />
					<span className={`avatar-status ${activeClass(row.status)}`}></span>
				</div>
			),
			busName: row.busName || <span>-</span>,
			busNumber: row.busNumber || <span>-</span>,
			busType: row.busType || <span>-</span>,
			driverName: row.driverName || <span>-</span>,
			driverNumber: row.driverNumber || <span>-</span>,
			status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
			seatType: row.seatType.seatType || <span>-</span>,
			routeName: row.routeName.routeName || <span>-</span>,
			// features: [],
			// images: row. || <span>-</span>,
			actions: (
				<>
					<Link to={`/route/${row._id}`}>
						<Button size="sm" className="btn-twitter btn-brand mr-1">
							<i className="fa fa-edit"></i>
						</Button>
					</Link>
					<Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
						<i className="fa fa-remove"></i>
					</Button>
				</>
			),
		};
	});

      {
        name: 'features',
        label: 'Features',
      },
      {
        name: 'driverName',
        label: 'Driver Name',
      },
      {
        name: 'status',
        label: 'Status',
      },

	const returnData = {
		columns: [
			{
				name: 'image',
				label: 'Photo',
			},
			{
				name: 'busName',
				label: 'Name',
			},
			{
				name: 'busNumber',
				label: 'Number',
				options: {
					filter: false,
					sort: false,
				},
			},
			{
				name: 'driverName',
				label: 'Driver Name',
			},
			{
				name: 'driverNumber',
				label: 'Driver Number',
			},
			
			{
				name: 'seatType',
				label: 'Seat Type',
			},
			{
				name: 'routeName',
				label: 'Route Name',
			},
			{
				name: 'status',
				label: 'Status',
			},
			{
				name: 'actions',
				label: 'Action',
				options: {
					filter: false,
					sort: false,
				},
			},
		],
		rows: formData,
		options: Options,
	};
	return returnData;
};
