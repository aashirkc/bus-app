import React from 'react';
import { getDocUrl } from '../../services/httpService';
import { Badge, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const activeClass = val => {
	let newClass = val === true ? 'badge-success' : 'badge-warning';
	return newClass;
};

const getBadge = status => {
	return status === 'true' ? 'success' : 'warning';
};

export const getRouteData = (data, handleDelete) => {
    console.log("sdfs",data)
	const formData = data.map(row => {
		return {
			routeName: row.routeName  || <span>-</span>,
			startPlace: row.startPlace || <span>-</span>,
			startTime: row.startTime || <span>-</span>,
			departureDay: row.departureDay || <span>-</span>,
            destinationPlace: row.destinationPlace || <span>-</span>,
            destinationTime: row.destinationTime || <span>-</span>,
            arrivalDay: row.arrivalDay || <span>-</span>,
            journeyTime: row.journeyTime || <span>-</span>,
            type: row.type || <span>-</span>,

			status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
			actions: (
				<>
					<Link to={`/route/${row._id}`}>
						<Button size="sm" className="btn-twitter btn-brand mr-1">
							<i className="fa fa-edit"></i>
						</Button>
					</Link>
					<Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
						<i className="fa fa-remove"></i>
					</Button>
				</>
			),
		};
	});

	const Options = {
		print: false,
		download: false,
		viewColumns: false,
		filterTable: false,
		filterType: false,
	};

	const returnData = {
		columns: [
			{
				name: 'routeName',
				label: 'Route Name',
				options: {
					filter: false,
					sort: false,
				},
			},
			{
				name: 'startPlace',
				label: 'Start Location',
			},
			{
				name: 'startTime',
				label: 'Start Time',
			},
			{
				name: 'departureDay',
				label: 'Leave Day',
			},
			{
				name: 'destinationPlace',
				label: 'Destination',
			},
            {
				name: 'destinationTime',
				label: 'Destination Time',
			},
              {
				name: 'arrivalDay',
				label: 'Arrival Day',
			},
             {
				name: 'journeyTime',
				label: 'Total time',
                width: '40px'
			},
             {
				name: 'type',
				label: 'Type',
			},
			{
				name: 'actions',
				label: 'Action',
				options: {
					filter: false,
					sort: false,
				},
			},
		],
		rows: formData,
		options: Options,
	};
	return returnData;
};
