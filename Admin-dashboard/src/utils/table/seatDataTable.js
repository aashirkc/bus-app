import React from 'react';
import { getDocUrl } from '../../services/httpService';
import { Badge, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const activeClass = val => {
	let newClass = val === true ? 'badge-success' : 'badge-warning';
	return newClass;
};

const getBadge = status => {
	return status === 'true' ? 'success' : 'warning';
};

export const getSeatData = (data, handleDelete) => {
    console.log("sdfs",data)
	const formData = data.map(row => {
		return {
			seatType: row.seatType || <span>-</span>,

			status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
			actions: (
				<>
					<Link to={`/route/${row._id}`}>
						<Button size="sm" className="btn-twitter btn-brand mr-1">
							<i className="fa fa-edit"></i>
						</Button>
					</Link>
					<Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
						<i className="fa fa-remove"></i>
					</Button>
				</>
			),
		};
	});

	const Options = {
		print: false,
		download: false,
		viewColumns: false,
		filterTable: false,
		filterType: false,
	};

	const returnData = {
		columns: [
			{
				name: 'seatType',
				label: 'Bus Type',
				options: {
					filter: true,
					sort: false,
				},
			},
			
			{
				name: 'actions',
				label: 'Action',
				options: {
					filter: false,
					sort: false,
				},
			},
		],
		rows: formData,
		options: Options,
	};
	return returnData;
};
