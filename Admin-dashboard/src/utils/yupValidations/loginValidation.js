import * as Yup from 'yup';

let password, stringReqValidate;

password = Yup.string().required('Password is required');

stringReqValidate = Yup.string().required('Required field');

const loginSchema = Yup.object().shape({
	userName: stringReqValidate,
	password: password,
});

export default loginSchema;
