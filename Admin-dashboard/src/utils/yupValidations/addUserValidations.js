import * as Yup from 'yup';

let emailValidate, password, passwordConfirmation, stringValidate, stringReqValidate;

// numberValidate = Yup.string()
// 	.matches(/^[1-9]+\d*$/, 'Must be a number')
// 	.required('Required field');

emailValidate = Yup.string()
	.email()
	.required('Email Required field');

password = Yup.string().required('Password is required');
passwordConfirmation = Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match');

stringValidate = Yup.string()
	.min(10, 'Too Short!')
	.max(10, 'Too Long!')
	.required('Required field');

stringReqValidate = Yup.string().required('Required field');

const addUserSchema = Yup.object().shape({
	fullName: stringReqValidate,
	userName: stringReqValidate,
	email: emailValidate,
	password: password,
	confirmPassword: passwordConfirmation,
	phoneNo: stringValidate,
});

export default addUserSchema;
