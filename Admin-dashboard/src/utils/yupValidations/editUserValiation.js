import * as Yup from 'yup';

let emailValidate, stringValidate, stringReqValidate;

emailValidate = Yup.string()
	.email()
	.required('Email Required field');

stringValidate = Yup.string()
	.min(10, 'Too Short!')
	.max(10, 'Too Long!')
	.required('Required field');

stringReqValidate = Yup.string().required('Required field');

const editUserSchema = Yup.object().shape({
	fullName: stringReqValidate,
	userName: stringReqValidate,
	email: emailValidate,
	phoneNo: stringValidate,
});

export default editUserSchema;
