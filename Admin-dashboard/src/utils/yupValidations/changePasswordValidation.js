import * as Yup from 'yup';

let stringValidate;

stringValidate = Yup.string()
	.min(5, 'Must be more than 5 characters')
	.required('This field is required');

const changePasswordSchema = Yup.object().shape({
	oldPass: stringValidate,
	newPass: stringValidate,
	newPassConfirm: Yup.string().when('newPass', {
		is: val => (val && val.length > 0 ? true : false),
		then: Yup.string().oneOf([Yup.ref('newPass')], 'Both password needs to be the same'),
	}),
});

export default changePasswordSchema;
