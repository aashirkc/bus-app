export const userOptions = [
	{ id: 1, value: 'admin', label: 'Admin' },
	{ id: 2, value: 'subadmin', label: 'Sub-Admin' },
	{ id: 3, value: 'user', label: 'User' },
];

export const activeOptions = [
	{ id: 1, value: true, label: 'Active' },
	{ id: 2, value: false, label: 'In Active' },
];

export const busTypeOptions = [
	{ id: 1, value: 'bus', label: 'Bus' },
	{ id: 2, value: 'Minibus', label: 'Minibus' },
];

export const bookedOptions = [
	{ id: 1, value: true, label: 'Yes' },
	{ id: 2, value: false, label: 'No' },
];

export const ColumnOptions = [
	{ id: 1, value: 'col1', label: 'C1' },
	{ id: 2, value: 'col2', label: 'C2' },
	{ id: 3, value: 'col3', label: 'C3' },
];

export const busFeatureOptions = [
	{ id: 1, value: 'wifi', label: 'Wi - Fi' },
	{ id: 2, value: 'ac/fan', label: 'A/C and Fan System' },
	{ id: 3, value: 'comSeats', label: 'Comfortable Seats' },
	{ id: 4, value: 'firstAid', label: 'First Aid Kits' },
	{ id: 5, value: 'charger', label: 'Mobile Charger' },
	{ id: 6, value: 'mineralWater', label: 'Mineral Water' },
	{ id: 7, value: 'staffs', label: 'Experienced and Friendly Staffs' },
	{ id: 8, value: 'departure', label: 'On Time Departure' },
];
