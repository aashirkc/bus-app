import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import './App.scss';
import { ToastContainer } from 'react-toastify';
import auth from './services/authService';
import loadable from '@loadable/component';

// Containers
const DefaultLayout = loadable(() => import('./containers/DefaultLayout'));

// Pages
const Login = loadable(() => import('./views/Pages/Login/Login'));
const Register = loadable(() => import('./views/Pages/Register'));
const Page404 = loadable(() => import('./views/Pages/Page404'));
const Page500 = loadable(() => import('./views/Pages/Page500'));

class App extends Component {
	render() {
		return (
			<HashRouter>
				<React.Fragment>
					<ToastContainer autoClose={3000} pauseOnVisibilityChange={false} draggable={false} pauseOnHover={false} pauseOnFocusLoss={false} />
					<Switch>
						<Route exact path="/login" name="Login Page" render={props => <Login {...props} />} />
						<Route exact path="/register" name="Register Page" render={props => <Register {...props} />} />
						<Route exact path="/404" name="Page 404" render={props => <Page404 {...props} />} />
						<Route exact path="/500" name="Page 500" render={props => <Page500 {...props} />} />
						<Route
							path="/"
							render={props => {
								if (!auth.getCurrentUser())
									return (
										<Redirect
											to={{
												pathname: '/login',
												state: { from: props.location },
											}}
										/>
									);
								return <DefaultLayout {...props} />;
							}}
						/>
					</Switch>
				</React.Fragment>
			</HashRouter>
		);
	}
}

export default App;
