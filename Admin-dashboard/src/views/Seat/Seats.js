import React, { Component } from 'react';
<<<<<<< HEAD
import { getSeats , deleteSeat} from '../../services/seatService';
=======
import { getSeat, deleteSeat } from '../../services/seatService';
import { getSeatData } from '../../utils/table/seatDataTable';
>>>>>>> 3769b48686667a19230f325f88415929c9b777d5
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';
<<<<<<< HEAD
import { getSeatData } from '../../utils/table/seatDataTable';

class Routes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {},
			seatData: [],
			errors: {},
			UserErrors: {},
			loading: true,
			dataResponse: false,
		};
	}

	async componentDidMount() {
		try {
			const res = await getSeats();
            console.log(res)
			this.setState({ seatData: res.data.payload, loading: false });
		} catch (err) {
			this.setState({ errors: err });
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: !prevState.loading });
		}
	}

	getBadge = status => {
		return status === 'true' ? 'success' : 'warning';
	};

	activeClass = val => {
		let newClass = val === true ? 'badge-success' : 'badge-warning';
		return newClass;
	};

	handleDelete = async user => {
		const originalUser = this.state.routeData;
		const SeatData = originalUser.filter(m => m._id !== user._id);
		this.setState({ SeatData });

		try {
			await deleteSeat(user._id);
			toast.success('Route deleted successfully.');
            // eslint-disable-next-line no-unused-expressions
            this.componentDidMount()
		} catch (ex) {
			if (ex.response && ex.response.status === 404) toast.error('This route has already been deleted.');
			this.setState({ UserData: originalUser, dataResponse: false, loading: true });
		}
	};

	render() {
		const { seatData, errors, loading } = this.state;

		const data = getSeatData(seatData, this.handleDelete);
		return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Seats Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
	}
}

export default Routes;
=======


class Seats extends Component {
  constructor(props) {
    super(props)


    this.state = {
      data: {},
      seatData: [],
      errors: {},
      SeatErrors: {},
      loading: true,
      dataResponse: false,
    }
  }



  async componentDidMount() {
    console.log("heey")
    try {
      const { data: seatData } = await getSeat()
      this.setState({ seatData, loading: false });
    } catch (err) {
      this.setState({ errors: err });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: !prevState.loading });
    }
  }

  handleDelete = async seat => {
    const originaluser = this.state.seatData;
    const seatData = originaluser.filter(m => m._id !== seat._id);
    this.setState({ seatData });

    try {
      await deleteSeat(seat._id);
      toast.success('Seat deleted successfully.');
    } catch (ex) {
      if (ex.response && ex.response.status === 404) toast.error('This seat has already been deleted.');
      this.setState({ seatData: originaluser, dataResponse: false, loading: true });
    }
  };



  getBadge = status => {
    return status === 'true' ? 'success' : 'warning';
  };

  activeClass = val => {
    let newClass = val === true ? 'badge-success' : 'badge-warning';
    return newClass;
  };

  render() {
    const { seatData, errors, loading } = this.state;

    const data = getSeatData(seatData, this.handleDelete);
    console.log(data)
    return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Seat Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
  }


}

export default Seats;

>>>>>>> 3769b48686667a19230f325f88415929c9b777d5
