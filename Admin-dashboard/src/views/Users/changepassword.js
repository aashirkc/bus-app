import React, { Component } from 'react';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { Formik, Field, ErrorMessage } from 'formik';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Button, Alert } from 'reactstrap';
import changePasswordSchema from '../../utils/yupValidations/changePasswordValidation';

class ChangePassword extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {
				oldPass: '',
				newPass: '',
				newPassConfirm: '',
			},
			hidden: {
				0: true,
				1: true,
				2: true,
			},
			names: {
				0: 'fa fa-eye-slash fa-lg',
				1: 'fa fa-eye-slash fa-lg',
				2: 'fa fa-eye-slash fa-lg',
			},
			errors: {},
			PassErrors: {},
			loading: false,
		};
	}

	handlePasswordChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	toggleShow = id => {
		this.setState(prevState => ({
			hidden: {
				...prevState.hidden,
				[id]: !prevState.hidden[id],
			},
		}));
		const hidden = this.state.hidden;
		if (hidden[id] === true) {
			let sub = 'fa fa-eye fa-lg';
			this.setState(prevState => ({
				names: {
					...prevState.names,
					[id]: sub,
				},
			}));
		} else {
			let sub = 'fa fa-eye-slash fa-lg';
			this.setState(prevState => ({
				names: {
					...prevState.names,
					[id]: sub,
				},
			}));
		}
	};

	render() {
		const { errors, loading, PassErrors } = this.state;
		return (
			<>
				{errors && errors.message ? (
					<NetworkErrorAlert />
				) : loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn">
						<Row>
							<Col xs="12">
								<Card>
									<CardHeader>
										<strong className="mr-2">Profile</strong>
										<small>Form</small>
									</CardHeader>
									<CardBody>
										{PassErrors && PassErrors.message && <Alert color="danger">{PassErrors.message}</Alert>}
										<Formik
											initialValues={this.state.data}
											validationSchema={changePasswordSchema}
											onSubmit={async (values, actions) => {
												actions.setSubmitting(true);
												console.log('here', values);
												// try {
												// 	const respose = await changePassword(values);
												// 	if (respose.data.error) throw new Error(respose.data.error.message);
												// 	else {
												// 		actions.setSubmitting(false);
												// 		toast.success('Password Changed successfully.');
												// 		actions.resetForm();
												// 	}
												// } catch (ex) {
												// 	if (ex.response && ex.response.status === 404) {
												// 		toast.error('Something went wrong.');
												// 	} else {
												// 		this.setState({ PassErrors: ex });
												// 	}
												// 	actions.setSubmitting(false);
												// }
											}}
										>
											{({ errors, handleSubmit, handleReset, handleChange, handleBlur, isSubmitting, loading }) => (
												<Form onSubmit={handleSubmit}>
													<Row>
														<Col lg="5" md="6">
															<FormGroup>
																<Label htmlFor="userName">Old Password</Label>
																<div className={'form-group'}>
																	<Row>
																		<Col lg="11" xs="10">
																			<Field
																				type={this.state.hidden[0] ? 'password' : 'text'}
																				name="oldPass"
																				className={errors.oldPass ? 'form-control is-invalid' : 'form-control'}
																				placeholder={'Enter Old Password'}
																				onChange={handleChange}
																				onBlur={handleBlur}
																			/>
																			{errors.oldPass && <ErrorMessage name="oldPass" component="div" className="invalid-feedback" />}
																		</Col>
																		<Col lg="1" xs="2">
																			<Button type="button" color="light" onClick={() => this.toggleShow(0)} className="new--button">
																				<i className={this.state.names[0]}></i>
																			</Button>
																		</Col>
																	</Row>
																</div>
															</FormGroup>
														</Col>
													</Row>
													<Row>
														<Col lg="5" md="6">
															<FormGroup>
																<Label htmlFor="userName">New Password</Label>
																<div className={'form-group'}>
																	<Row>
																		<Col lg="11" xs="10">
																			<Field
																				type={this.state.hidden[1] ? 'password' : 'text'}
																				name="newPass"
																				className={errors.newPass ? 'form-control is-invalid' : 'form-control'}
																				placeholder={'Enter New Password'}
																				onChange={handleChange}
																				onBlur={handleBlur}
																			/>
																			{errors.newPass && <ErrorMessage name="newPass" component="div" className="invalid-feedback" />}
																		</Col>
																		<Col lg="1" xs="2">
																			<Button type="button" color="light" onClick={() => this.toggleShow(1)} className="new--button">
																				<i className={this.state.names[1]}></i>
																			</Button>
																		</Col>
																	</Row>
																</div>
															</FormGroup>
														</Col>
													</Row>
													<Row>
														<Col lg="5" md="6">
															<FormGroup>
																<Label htmlFor="userName">Confirm Password</Label>
																<div className="new-form-control">
																	<Row>
																		<Col lg="11" xs="10">
																			<Field
																				type={this.state.hidden[2] ? 'password' : 'text'}
																				name="newPassConfirm"
																				className={errors.newPassConfirm ? 'form-control is-invalid' : 'form-control'}
																				placeholder={'Enter Confirm Password'}
																				onChange={handleChange}
																				onBlur={handleBlur}
																			/>
																			{errors.newPassConfirm && <ErrorMessage name="newPassConfirm" component="div" className="invalid-feedback" />}
																		</Col>
																		<Col lg="1" xs="2">
																			<Button type="button" color="light" onClick={() => this.toggleShow(2)} className="new--button">
																				<i className={this.state.names[2]}></i>
																			</Button>
																		</Col>
																	</Row>
																</div>
															</FormGroup>
														</Col>
													</Row>
													<Row>
														<Col xs="6">
															<Button type="button" onClick={handleSubmit} color="primary" className="px-4 mt-3">
																Add
															</Button>
														</Col>
													</Row>
												</Form>
											)}
										</Formik>
									</CardBody>
								</Card>
							</Col>
						</Row>
					</div>
				)}
			</>
		);
	}
}

export default ChangePassword;
