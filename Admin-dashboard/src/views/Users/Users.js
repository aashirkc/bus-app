import React, { Component } from 'react';
import { getUsers, deleteUser } from '../../services/addUserServices';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';
import { getUserData } from '../../utils/table/userDataTable';

class Users extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {},
			UserData: [],
			errors: {},
			UserErrors: {},
			loading: true,
			dataResponse: false,
		};
	}

	async componentDidMount() {
		try {
			const { data: UserData } = await getUsers();
			this.setState({ UserData, loading: false });
		} catch (err) {
			this.setState({ errors: err });
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: !prevState.loading });
		}
	}

	getBadge = status => {
		return status === 'true' ? 'success' : 'warning';
	};

	activeClass = val => {
		let newClass = val === true ? 'badge-success' : 'badge-warning';
		return newClass;
	};

	handleDelete = async user => {
		const originaluser = this.state.UserData;
		const UserData = originaluser.filter(m => m._id !== user._id);
		this.setState({ UserData });

		try {
			await deleteUser(user._id);
			toast.success('User deleted successfully.');
		} catch (ex) {
			if (ex.response && ex.response.status === 404) toast.error('This User has already been deleted.');
			this.setState({ UserData: originaluser, dataResponse: false, loading: true });
		}
	};

	render() {
		const { UserData, errors, loading } = this.state;

		const data = getUserData(UserData, this.handleDelete);
		return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'User Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
	}
}

export default Users;
