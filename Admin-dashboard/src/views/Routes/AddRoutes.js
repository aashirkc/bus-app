import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Button, Alert } from 'reactstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { userOptions, activeOptions } from '../../utils/options/Options';
import { addRoute , getRoute, getDistrict} from '../../services/addrouteService';
import { toast } from 'react-toastify';
// import {districts} from 'nepal-geojson'
import Loader from '../../common/loader';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';

class Routes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			errors: {},
			data: {
				id: '',
				routeName: '',
				startDistrict: '',
				endDistrict: '',
				type: '',
			},
			district: [],
			isedit: false,
			AddRoutesErrors: {},
			dataResponse: false,
		};
	}

	async editUser() {
		try {
			const id = this.props.match.params.id;
			if (!id) {
				this.setState({ loading: false });
				return;
			}

			const { data: userdata } = await getRoute(id);

			this.setState({ data: this.mapToViewModal(userdata), loading: false, isedit: true });
		} catch (ex) {
			if (ex.response && ex.response.status === 404) return this.props.history.replace('/404');

			this.setState({ errors: ex, loading: false });
		}
	}

	async componentDidMount() {
		let dis = await getDistrict()
		this.setState({district: dis.data.payload.features})
		console.log(this.state.district)
		await this.editUser();
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: false });
		}
	}

	mapToViewModal(route) {
		return {
			id: route._id || '',
			routeName: route.routeName || '',
			startDistrict: route.startDistrict || '',
			endDistrict:  route.endDistrict ||''
		};
	}

	render() {
		const { AddRoutesErrors, errors, loading } = this.state;

		return (
			<>
				{errors && errors.message ? (
					<NetworkErrorAlert />
				) : loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn">
						<Row>
							<Col xs="12">
								<Card>
									<CardHeader>
										<strong className="mr-2">Add Routes</strong>
										<small>Form</small>
									</CardHeader>
									<CardBody>
										{AddRoutesErrors && AddRoutesErrors.message && <Alert color="danger">{AddRoutesErrors.message}</Alert>}
										<Formik
											initialValues={this.state.data}
											onSubmit={async (values, actions) => {
												actions.setSubmitting(true);
                                                console.log(values)
												try {
													const response = await addRoute(values);
													if (response.data.error) throw new Error(response.data.error);
													else {
														toast.success('Added successfully!');
													}
													actions.resetForm();
													actions.setSubmitting(false);
													this.setState({ dataResponse: true, AddUserErrors: {}, loading: true });
												} catch (ex) {
													if (ex.response && ex.response.status === 404) {
														toast.error('Something went wrong.');
													} else {
														this.setState({ AddUserErrors: ex });
													}
													actions.setSubmitting(false);
												}
											}}
										>
											{({ values, handleChange, isSubmitting, handleSubmit, handleBlur, errors, setFieldValue }) => (
												<Form onSubmit={handleSubmit}>
													<Row>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="routeName">Route Name</Label>
																<Field type="text" name="routeName" className={errors.routeName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Route Name'} autoComplete="off" />
																{errors.fullName && <ErrorMessage name="route Name" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="startDistrict">Start District</Label>
																	<select name="startDistrict" value={values.startDistrict} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Route Name'} value={''} defaultChecked />
																	{this.state.district && this.state.district.map(Type => (
																		<option key={Type.id} value={Type.properties.DISTRICT} label={Type.properties.DISTRICT} />
																	))}
																</select>
																{errors.startDistrict && <span className="invalid-feedback">{errors.startDistrict}</span>}
															</FormGroup>
														</Col>

														
                                                        <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="endDistrict">End District</Label>
																	<select name="endDistrict" value={values.endDistrict} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Route Name'} value={''} defaultChecked />
																	{this.state.district && this.state.district.map(Type => (
																		<option key={Type.id} value={Type.properties.DISTRICT} label={Type.properties.DISTRICT} />
																	))}
																</select>
																{errors.endDistrict && <span className="invalid-feedback">{errors.endDistrict}</span>}
															</FormGroup>
														</Col>

                                                       

                                                

                                                        

														
													</Row>
													<Row>
														<Col xs="6">
															<Button type="button" onClick={handleSubmit} color="primary" className="px-4 mt-3">
																Add Route
															</Button>
														</Col>
													</Row>
												</Form>
											)}
										</Formik>
									</CardBody>
								</Card>
							</Col>
						</Row>
					</div>
				)}
			</>
		);
	}
}

export default Routes;


