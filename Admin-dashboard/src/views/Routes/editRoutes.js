import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Button, Alert } from 'reactstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { userOptions, activeOptions } from '../../utils/options/Options';
import { addRoute , getRoute, editRoute} from '../../services/addrouteService';
import { toast } from 'react-toastify';
import Loader from '../../common/loader';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';

class EditRoutes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			errors: {},
			data: {
				id: '',
				routeName: '',
				startPlace: '',
				startTime: '',
				destinationPlace: '',
				destinationTime: '',
				departureDay: '',
                arrivalDay:'',
				journeyTime: '',
				type: '',
			},
			isedit: false,
			AddRoutesErrors: {},
			dataResponse: false,
		};
	}

	async editUser() {
		try {
			const id = this.props.match.params.id;
			if (!id) {
				this.setState({ loading: false });
				return;
			}

			const data = await getRoute(id);

			this.setState({ data: this.mapToViewModal(data.data.payload), loading: false, isedit: true });
		} catch (ex) {
			if (ex.response && ex.response.status === 404) return this.props.history.replace('/404');

			this.setState({ errors: ex, loading: false });
		}
	}

	async componentDidMount() {
		await this.editUser();
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: false });
		}
	}

	mapToViewModal(route) {
		return {
			id: route._id || '',
			routeName: route.routeName || '',
			startPlace: route.startPlace || '',
			startTime: route.startTime || '',
			destinationPlace: route.destinationPlace || '',
			destinationTime: route.destinationTime || '',
			departureDay: route.departureDay || '',
            arrivalDay: route.arrivalDay || '',
			journeyTime: route.journeyTime || '',
			type: route.type || '',
		};
	}

	render() {
		const { AddRoutesErrors, errors, loading } = this.state;

		return (
			<>
				{errors && errors.message ? (
					<NetworkErrorAlert />
				) : loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn">
						<Row>
							<Col xs="12">
								<Card>
									<CardHeader>
										<strong className="mr-2">Edit Routes</strong>
										<small>Form</small>
									</CardHeader>
									<CardBody>
										{AddRoutesErrors && AddRoutesErrors.message && <Alert color="danger">{AddRoutesErrors.message}</Alert>}
										<Formik
											initialValues={this.state.data}
											onSubmit={async (values, actions) => {
												actions.setSubmitting(true);
												try {
													const response = await editRoute(this.props.match.params.id, values);
													if (response.data.error) throw new Error(response.data.error);
													else {
														toast.success('Added successfully!');
													}
													actions.resetForm();
													actions.setSubmitting(false);
													this.setState({ dataResponse: true, AddUserErrors: {}, loading: true });
												} catch (ex) {
													if (ex.response && ex.response.status === 404) {
														toast.error('Something went wrong.');
													} else {
														this.setState({ AddUserErrors: ex });
													}
													actions.setSubmitting(false);
												}
											}}
										>
											{({ values, handleChange, isSubmitting, handleSubmit, handleBlur, errors, setFieldValue }) => (
												<Form onSubmit={handleSubmit}>
													<Row>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="routeName">Route Name</Label>
																<Field type="text" name="routeName" className={errors.routeName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Route Name'} autoComplete="off" />
																{errors.fullName && <ErrorMessage name="route Name" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="startPlace">Start Location</Label>
																<Field type="text" name="startPlace" className={errors.startPlace ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter start location'} autoComplete="off" />
																{errors.startPlace && <ErrorMessage name="startPlace" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="startTime">Start Time</Label>
																<Field type="text" name="startTime" className={errors.startTime ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter start time'} autoComplete="off" />
																{errors.startTime && <ErrorMessage name="startTime" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

                                                         <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="departureDay">Departure Day</Label>
																<Field type="text" name="departureDay" className={errors.departureDay ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter departure day'} autoComplete="off" />
																{errors.departureDay && <ErrorMessage name="departureDay" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="destinationPlace">Destination Place</Label>
																<Field type="text" name="destinationPlace" className={errors.destinationPlace ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter destination place'} autoComplete="off" />
																{errors.destinationPlace && <ErrorMessage name="destinationPlace" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
                                                        <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="destinationTime">Destination Time</Label>
																<Field type="text" name="destinationTime" className={errors.destinationTime ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter destination time'} autoComplete="off" />
																{errors.destinationTime && <ErrorMessage name="destinationTime" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

                                                        <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="arrivalDay">Arrival Day</Label>
																<Field type="text" name="arrivalDay" className={errors.arrivalDay ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter departure day'} autoComplete="off" />
																{errors.arrivalDay && <ErrorMessage name="arrivalDay" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

                                                        <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="journeyTime">Journey Time</Label>
																<Field type="text" name="journeyTime" className={errors.journeyTime ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter time from start to end'} autoComplete="off" />
																{errors.journeyTime && <ErrorMessage name="journeyTime" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

                                                        <Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="type" className="mr-2">
																	Type
																</Label>
																<select name="type" value={values.type} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Status'} value={''} defaultChecked />
                                                                    <option label={'one-way'} value={'one-way'}  />
                                                                    <option label={'two-way'} value={'two-way'}  />
													
																</select>
																{errors.type && <span className="invalid-feedback">{errors.type}</span>}
															</FormGroup>
														</Col>


														
													</Row>
													<Row>
														<Col xs="6">
															<Button type="button" onClick={handleSubmit} color="primary" className="px-4 mt-3">
																Edit Route
															</Button>
														</Col>
													</Row>
												</Form>
											)}
										</Formik>
									</CardBody>
								</Card>
							</Col>
						</Row>
					</div>
				)}
			</>
		);
	}
}

export default EditRoutes;


