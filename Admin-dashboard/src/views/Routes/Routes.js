import React, { Component } from 'react';
import { getRoutes , deleteRoute} from '../../services/addrouteService';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';
import { getRouteData } from '../../utils/table/routeDataTable';
import { Redirect } from 'react-router-dom';

class Routes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {},
			routeData: [],
			errors: {},
			UserErrors: {},
			loading: true,
			dataResponse: false,
		};
	}

	async componentDidMount() {
		try {
			const res = await getRoutes();
			this.setState({ routeData: res.data.payload, loading: false });
		} catch (err) {
			this.setState({ errors: err });
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: !prevState.loading });
		}
	}

	getBadge = status => {
		return status === 'true' ? 'success' : 'warning';
	};

	activeClass = val => {
		let newClass = val === true ? 'badge-success' : 'badge-warning';
		return newClass;
	};

	handleDelete = async user => {
		const originalUser = this.state.routeData;
		const RouteData = originalUser.filter(m => m._id !== user._id);
		this.setState({ RouteData });

		try {
			await deleteRoute(user._id);
			toast.success('Route deleted successfully.');
            // eslint-disable-next-line no-unused-expressions
            this.componentDidMount()
		} catch (ex) {
			if (ex.response && ex.response.status === 404) toast.error('This route has already been deleted.');
			this.setState({ UserData: originalUser, dataResponse: false, loading: true });
		}
	};

	render() {
		const { routeData, errors, loading } = this.state;

		const data = getRouteData(routeData, this.handleDelete);
		return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Route Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
	}
}

export default Routes;
