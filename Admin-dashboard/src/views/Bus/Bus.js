import React, { Component } from 'react';
import {  deleteBus, getBuses } from '../../services/busServices';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';
import { getBusData } from '../../utils/table/busDataTable';

class ManageBus extends Component {
<<<<<<< HEAD
	constructor(props) {
		super(props);

		this.state = {
			data: {},
			BusData: [],
			errors: {},
			UserErrors: {},
			loading: true,
			dataResponse: false,
		};
	}

	async componentDidMount() {
		try {
			const  res  = await getBuses();
			console.log(res)
			this.setState({ BusData: res.data.payload, loading: false });
		} catch (err) {
			this.setState({ errors: err });
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			this.setState({ loading: !prevState.loading });
		}
	}

	handleDelete = async bus => {
		const originalbus = this.state.BusData;
		const BusData = originalbus.filter(m => m._id !== bus._id);
		this.setState({ BusData });

		try {
			await deleteBus(bus._id);
			toast.success('Bus deleted successfully.');
		} catch (ex) {
			if (ex.response && ex.response.status === 404) toast.error('This Bus has already been deleted.');
			this.setState({ BusData: originalbus, dataResponse: false, loading: true });
		}
	};

	render() {
		const {BusData, errors, loading } = this.state;

		const data = getBusData(BusData, this.handleDelete);
		return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Bus Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
	}
=======
  constructor(props) {
    super(props);
    console.log(this.props)

    this.state = {
      data: {},
      BusData: [],
      errors: {},
      UserErrors: {},
      loading: true,
      dataResponse: false,
    };
  }

  async componentDidMount() {
    try {
      // const beforeParse = await getBus();
      // console.log(beforeParse)
      // beforeParse.map(x =>
      //   console.log(x))

      const { data: BusData } = await getBus();
      BusData.map(x => {
        console.log(x)
        x.features = JSON.parse(x.features)
        return x
      })

      console.log(BusData)
      this.setState({ BusData, loading: false });

    } catch (err) {
      this.setState({ errors: err });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: !prevState.loading });
    }
  }

  handleDelete = async bus => {
    const originalbus = this.state.BusData;
    const BusData = originalbus.filter(m => m._id !== bus._id);
    this.setState({ BusData });

    try {
      await deleteBus(bus._id);
      toast.success('Bus deleted successfully.');
    } catch (ex) {
      if (ex.response && ex.response.status === 404) toast.error('This Bus has already been deleted.');
      this.setState({ BusData: originalbus, dataResponse: false, loading: true });
    }
  };

  render() {
    const { BusData, errors, loading } = this.state;

    const data = getBusData(BusData, this.handleDelete);
    return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Bus Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
  }
>>>>>>> 3769b48686667a19230f325f88415929c9b777d5
}

export default ManageBus;
