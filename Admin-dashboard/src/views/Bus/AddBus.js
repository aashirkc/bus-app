import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Alert, Button } from 'reactstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { busTypeOptions, activeOptions, busFeatureOptions } from '../../utils/options/Options';
import { addBus } from '../../services/busServices';

import {getSeats} from '../../services/seatService';
import { getRoutes } from '../../services/addrouteService';
import { toast } from 'react-toastify';
import Loader from '../../common/loader';
import { getSeat } from '../../services/seatService';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';

class User extends Component {
  constructor(props) {
    super(props);

<<<<<<< HEAD
		this.state = {
			loading: true,
			errors: {},
			seats:[],
			routes:[],
			data: {
				busName: '',
				busNumber: '',
				busType: '',
				driverName: '',
				driverNumber: '',
				startPlace: '',
				departureDay: '',
				destinationPlace: '',
				arrivalDay: '',
				type: '',
				status: '',
				seatType: '',
				routeName: '',
				features: [],
				images: '',
				file: '',
			},
			AddBusErrors: {},
			checkedArray: [],
			dataResponse: false,
		};
	}

	async componentDidMount() {
		try {
			const Seat = await getSeats()
			this.state.seats = Seat.data.payload
			const Route = await getRoutes()
			this.state.routes = Route.data.payload
			console.log("Route",Route)
			console.log("Seat",Seat)
			this.setState({ loading: false });
		} catch (err) {
			this.setState({ errors: err });
		}
	}
=======
    this.state = {
      loading: true,
      errors: {},
      data: {
        busName: '',
        busNo: '',
        busType: '',
        driverName: '',
        status: '',
        seatType: '',
        features: [],
        image: '',
        file: [],
      },
      AddBusErrors: {},
      checkedArray: [],

      dataResponse: false,
    };
  }
>>>>>>> 3769b48686667a19230f325f88415929c9b777d5

  async componentDidMount() {
    try {
      const { data: seatData } = await getSeat()
      console.log(seatData)
      this.setState(prevState => ({ seatData, loading: !prevState.loading }));

<<<<<<< HEAD
	handleSeatChange = val => {
		// const { features } = this.state.data;
		this.setState(prevstate => {
			const prevCheckedArray = prevstate.data.features;
			let retCheckedArray;
			if (prevCheckedArray.find(rw => rw.id === val.id)) {
				retCheckedArray = prevCheckedArray.filter(row => row.id !== val.id);
			} else {
				retCheckedArray = prevCheckedArray.concat(val);
			}	
			console.log(JSON.stringify(retCheckedArray))
			
			
			return {
				data: { ...this.state.data, features: retCheckedArray },
			};
		});
	};
=======
    } catch (err) {
      this.setState({ errors: err });
    }
  }
>>>>>>> 3769b48686667a19230f325f88415929c9b777d5

  async componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: false });
    }
  }

<<<<<<< HEAD
								
								
								values.features = JSON.stringify(this.state.data.features);	
								values.status = JSON.parse(values.status);
								

								const formData = new FormData();
								formData.append('busName', values.busName);
								formData.append('busNumber', values.busNumber);
								formData.append('seatType', values.seatType);
								formData.append('driverName', values.driverName);
								formData.append('driverNumber', values.driverNumber);
								formData.append('startPlace', values.startPlace);
								formData.append('departureDay', values.departureDay);
								formData.append('destinationPlace', values.destinationPlace);
								formData.append('arrivalDay', values.arrivalDay);
								formData.append('type', values.type);
								formData.append('status', values.status);
								formData.append('routeName', values.routeName);
								formData.append('features', values.features);
								formData.append('images', values.images);

								try {
									const response = await addBus(formData);
									if (response.data.error) throw new Error(response.data.error);
									else {
										toast.success('Added successfully!');
										this.setState({ dataResponse: true, AddBusErrors: {}, loading: true });
									}
									actions.resetForm();
									actions.setSubmitting(false);
								} catch (ex) {
									if (ex.response && ex.response.status === 404) {
										toast.error('Something went wrong.');
									} else {
										this.setState({ AddBusErrors: ex });
									}
									actions.setSubmitting(false);
								}
							}}
						>
							{({ values, handleChange, handleSubmit, handleBlur, errors, setFieldValue }) => (
								<Form>
									<Row>
										<Col lg="9">
											<Card>
												<CardHeader>
													<strong className="mr-2">Bus Detail</strong>
													<small>Form</small>
												</CardHeader>
												<CardBody>
													<Row>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="busName">Bus Name</Label>
																<Field type="text" name="busName" className={errors.busName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Name'} autoComplete="off" />
																{errors.busName && <ErrorMessage name="busName" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="busNumber">Bus Number</Label>
																<Field type="text" name="busNumber" className={errors.busNumber ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Number'} autoComplete="off" />
																{errors.busNumber && <ErrorMessage name="busNumber" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="busType">Bus Type</Label>
																<select name="seatType" value={values.seatType} onChange={handleChange} className={'form-control'}>
																	<option label={'Select seatType'} value="" defaultChecked />
																	{this.state.seats && this.state.seats.map(user => (
																		<option key={user._id} value={user._id} label={user.seatType} />
																	))}
																</select>
																{errors.seatType && <span className="invalid-feedback">{errors.seatType}</span>}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="driverName">Driver Name</Label>
																<Field
																	type="text"
																	name="driverName"
																	className={errors.driverName ? 'form-control is-invalid' : 'form-control'}
																	placeholder={"Enter Driver's Name"}
																	autoComplete="off"
																/>
																{errors.driverName && <ErrorMessage name="driverName" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="driverName">Driver Number</Label>
																<Field
																	type="number"
																	name="driverNumber"
																	className={errors.driverNumber ? 'form-control is-invalid' : 'form-control'}
																	placeholder={"Enter Driver's Name"}
																	autoComplete="off"
																/>
																{errors.driverNumber && <ErrorMessage name="driverNumber" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="startPlace">Start Location</Label>
																<Field type="text" name="startPlace" className={errors.startPlace ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter start location'} autoComplete="off" />
																{errors.startPlace && <ErrorMessage name="startPlace" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

                                                         <Col md="6">
															<FormGroup>
																<Label htmlFor="departureDay">Departure Day</Label>
																<Field type="text" name="departureDay" className={errors.departureDay ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter departure day'} autoComplete="off" />
																{errors.departureDay && <ErrorMessage name="departureDay" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="destinationPlace">Destination Place</Label>
																<Field type="text" name="destinationPlace" className={errors.destinationPlace ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter destination place'} autoComplete="off" />
																{errors.destinationPlace && <ErrorMessage name="destinationPlace" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														 <Col  md="6">
															<FormGroup>
																<Label htmlFor="arrivalDay">Arrival Day</Label>
																<Field type="text" name="arrivalDay" className={errors.arrivalDay ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter departure day'} autoComplete="off" />
																{errors.arrivalDay && <ErrorMessage name="arrivalDay" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>

														<Col md="6">
															<FormGroup>
																<Label htmlFor="status" className="mr-2">
																	Route Name
																</Label>
																<select name="routeName" value={values.routeName} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Route Name'} value={''} defaultChecked />
																	{this.state.routes && this.state.routes.map(Type => (
																		<option key={Type._id} value={Type._id} label={Type.routeName} />
																	))}
																</select>
																{errors.routeName && <span className="invalid-feedback">{errors.routeName}</span>}
															</FormGroup>
														</Col>
														<Col md="6">
															<FormGroup>
																<Label htmlFor="type" className="mr-2">
																	Type
																</Label>
																<select name="type" value={values.type} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Status'} value={''} defaultChecked />
                                                                    <option label={'one-way'} value={'one-way'}  />
                                                                    <option label={'two-way'} value={'two-way'}  />
													
																</select>
																{errors.type && <span className="invalid-feedback">{errors.type}</span>}
															</FormGroup>
														</Col>

														<Col md="6">
															<FormGroup>
																<Label htmlFor="status" className="mr-2">
																	Status
																</Label>
																<select name="status" value={values.status} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Status'} value={''} defaultChecked />
																	{activeOptions.map(status => (
																		<option key={status.id} value={status.value} label={status.label} />
																	))}
																</select>
																{errors.status && <span className="invalid-feedback">{errors.status}</span>}
															</FormGroup>
														</Col>
													
													</Row>
												</CardBody>
											</Card>
											<Card>
												<CardHeader>
													<strong className="mr-2">Bus Image</strong>
													<small>Form</small>
												</CardHeader>
												<CardBody>
													<Col lg="6" md="6">
														<FormGroup>
															<FormGroup>
																<Label htmlFor="images">Select Image</Label>
																<input
																	type="file"
																	name="images"
																	onBlur={handleBlur}
																	onChange={e => {
																		setFieldValue('images', e.target.files[0]);
																		console.log(this.state.data.images)
																	}}
																	className={errors.images ? 'form-control is-invalid' : 'form-control'}
																/>
																{errors.images && <ErrorMessage name="images" component="div" className="invalid-feedback" />}
															</FormGroup>
														</FormGroup>
													</Col>
													<Col lg="12" md="12">
														<FormGroup>
															<div className="bus_image--wrapper">
																{values.file &&
																	values.file.map((value, index) => (
																		<div className="bus_image" key={index}>
																			<img src={value.src} alt="BusImages" />
																		</div>
																	))}
															</div>
														</FormGroup>
													</Col>
												</CardBody>
											</Card>
										</Col>
										<Col lg="3">
											<Card>
												<CardHeader>
													<strong className="mr-2">Bus Features</strong>
													<small>Form</small>
												</CardHeader>
												<CardBody>
													<FormGroup>
														<Col md="12">
															{busFeatureOptions.map(m => (
																<FormGroup check className="checkbox pb-3" key={m.id}>
																	<Field
																		name="features"
																		onChange={() => this.handleSeatChange(m)}
																		className={errors.features ? 'form-check-input is-invalid' : 'form-check-input'}
																		type="checkbox"
																		id={m.id}
																		value={values.features}
																	/>
																	<Label check className="form-check-label" htmlFor={m.id}>
																		{m.label}
																	</Label>
																</FormGroup>
															))}
															{errors.features && <ErrorMessage name="features" component="div" className="invalid-feedback" />}
														</Col>
													</FormGroup>
												</CardBody>
											</Card>
										</Col>
									</Row>
									<Row>
										<Col xs="6">
											<Button type="button" onClick={handleSubmit} color="primary" className="px-4 mb-3">
												Add Bus
=======
  handleSeatChange = (val, index) => {

    console.log(val, index)
    // console.log(event)

    const { features } = this.state.data;
    this.setState(prevstate => {
      const prevCheckedArray = prevstate.data.features;
      let retCheckedArray;
      if (prevCheckedArray.find(rw => rw.id === val.id)) {
        retCheckedArray = prevCheckedArray.filter(row => row.id !== val.id);
        console.log("1", retCheckedArray)
        this.state.data.features = retCheckedArray
      } else {
        retCheckedArray = prevCheckedArray.concat(val);
        console.log("2", retCheckedArray)
        this.state.data.features = retCheckedArray
      }
      return {
        data: { ...this.state.data },
      };
    });



    // const featured_list = this.state.data.features;
    // let check = event.target.checked;
    // let checked_features = event.target.value;
    // console.log(checked_features)
    // if (check) {
    //   this.setState({
    //     data: { features: [...this.state.data.features, checked_features] }

    //   })
    //   console.log("1", this.state.data.features)

    // } else {
    //   var index = featured_list.indexOf(checked_features);
    //   console.log("2", this.state.data.features)
    //   if (index > -1) {
    //     featured_list.splice(index, 1);
    //     this.setState({
    //       data: { features: featured_list }
    //     })
    //     console.log("3", this.state.data.features)
    //   }
    // }
  }



  render() {
    const { seatData, AddBusErrors, errors, loading } = this.state;
    return (
      <>
        {errors && errors.message ? (
          <NetworkErrorAlert />
        ) : loading ? (
          <Loader />
        ) : (
              <div className="animated fadeIn">
                {AddBusErrors && AddBusErrors.message && <Alert color="danger">{AddBusErrors.message.message}</Alert>}
                <Formik
                  initialValues={this.state.data}
                  onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);



                    values.features = JSON.stringify(this.state.data.features);
                    //  values.status = JSON.parse(values.status);
                    // values.seatType = JSON.parse(values.seatType);

                    console.log(values);



                    const formData = new FormData();
                    formData.append('busName', values.busName);
                    formData.append('busNo', values.busNo);
                    formData.append('busType', values.busType);
                    formData.append('driverName', values.driverName);
                    formData.append('status', values.status);
                    formData.append('seatType', values.seatType);
                    formData.append('features', values.features);
                    formData.append('image', values.image);

                    console.log(formData)

                    try {
                      const response = await addBus(formData);
                      if (response.data.error) throw new Error(response.data.error);
                      else {
                        toast.success('Added successfully!');
                        this.setState({ dataResponse: true, AddBusErrors: {}, loading: true });
                      }
                      actions.resetForm();
                      actions.setSubmitting(false);
                    } catch (ex) {
                      if (ex.response && ex.response.status === 404) {
                        toast.error('Something went wrong.');
                      } else {
                        this.setState({ AddBusErrors: ex });
                      }
                      actions.setSubmitting(false);
                    }
                  }}
                >
                  {({ values, handleChange, handleSubmit, handleBlur, errors, setFieldValue }) => (
                    <Form>
                      <Row>
                        <Col lg="9">
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Detail</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <Row>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busName">Bus Name</Label>
                                    <Field type="text" name="busName" className={errors.busName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Name'} autoComplete="off" />
                                    {errors.busName && <ErrorMessage name="busName" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busNo">Bus Number</Label>
                                    <Field type="text" name="busNo" className={errors.busNo ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Number'} autoComplete="off" />
                                    {errors.busNo && <ErrorMessage name="busNo" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busType">Bus Type</Label>
                                    <select name="busType" value={values.busType} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select busType'} value="" defaultChecked />
                                      {busTypeOptions.map(user => (
                                        <option key={user.id} value={user.value} label={user.label} />
                                      ))}
                                    </select>
                                    {errors.busType && <span className="invalid-feedback">{errors.busType}</span>}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="driverName">Driver Name</Label>
                                    <Field
                                      type="text"
                                      name="driverName"
                                      className={errors.driverName ? 'form-control is-invalid' : 'form-control'}
                                      placeholder={"Enter Driver's Name"}
                                      autoComplete="off"
                                    />
                                    {errors.driverName && <ErrorMessage name="driverName" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="status" className="mr-2">
                                      Status
																</Label>
                                    <select name="status" value={values.status} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select Status'} value={''} defaultChecked />
                                      {activeOptions.map(status => (
                                        <option key={status.id} value={status.value} label={status.label} />
                                      ))}
                                    </select>
                                    {errors.status && <span className="invalid-feedback">{errors.status}</span>}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="status" className="mr-2">
                                      Seat Type
																</Label>
                                    <select name="seatType" value={values.seatType} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select Seat Type'} value={''} defaultChecked />
                                      {
                                        seatData.map(
                                          ta => <option key={ta._id} value={ta._id} label={ta.busType}></option>
                                        )
                                      }
                                    </select>
                                    {errors.seatType && <span className="invalid-feedback">{errors.seatType}</span>}
                                  </FormGroup>
                                </Col>
                              </Row>
                            </CardBody>
                          </Card>
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Features</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <Col lg="6" md="6">
                                <FormGroup>
                                  <FormGroup>
                                    <Label htmlFor="images">Select Image</Label>
                                    <input
                                      type="file"
                                      name="image"
                                      onBlur={handleBlur}
                                      onChange={e => {
                                        setFieldValue('image', e.target.files[0]);

                                        const src = window.URL.createObjectURL(e.target.files[0]);
                                        setFieldValue('file', src);
                                      }}

                                      className={errors.image ? 'form-control is-invalid' : 'form-control'}
                                    />
                                    {errors.image && <ErrorMessage name="image" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </FormGroup>
                              </Col>
                              <Col lg="12" md="12">
                                <FormGroup>
                                  <div className="bus_image--wrapper">
                                    {values.file && (
                                      <div className="user-avatar">
                                        <img src={values.file} alt="UserImage" />
                                      </div>
                                    )}
                                  </div>
                                </FormGroup>
                              </Col>
                            </CardBody>
                          </Card>
                        </Col>
                        <Col lg="3">
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Features</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <FormGroup>
                                <Col md="12">
                                  {busFeatureOptions.map((m, index) => (
                                    <FormGroup check className="checkbox pb-3" key={m.id}>
                                      <Field
                                        name="features"
                                        onChange={() => this.handleSeatChange(m, index)}
                                        className={errors.features ? 'form-check-input is-invalid' : 'form-check-input'}
                                        type="checkbox"
                                        id={m.id}
                                        value={values.features}
                                      />
                                      <Label check className="form-check-label" htmlFor={m.id}>
                                        {m.label}
                                      </Label>
                                    </FormGroup>
                                  ))}
                                  {errors.features && <ErrorMessage name="features" component="div" className="invalid-feedback" />}
                                </Col>
                              </FormGroup>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button type="button" onClick={handleSubmit} color="primary" className="px-4 mb-3">
                            Add Bus
>>>>>>> 3769b48686667a19230f325f88415929c9b777d5
											</Button>
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </div>
            )}
      </>
    );
  }
}

export default User;
