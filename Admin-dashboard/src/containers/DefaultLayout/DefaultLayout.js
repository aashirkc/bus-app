import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';

import {
	AppAside,
	AppFooter,
	AppHeader,
	AppSidebar,
	AppSidebarFooter,
	AppSidebarForm,
	AppSidebarHeader,
	AppSidebarMinimizer,
	AppBreadcrumb2 as AppBreadcrumb,
	AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import loadable from '@loadable/component';

const DefaultAside = loadable(() => import('./DefaultAside'));
const DefaultFooter = loadable(() => import('./DefaultFooter'));
const DefaultHeader = loadable(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
	signOut(e) {
		e.preventDefault();
		this.props.history.push('/login');
	}

	render() {
		return (
			<div className="app">
				<AppHeader fixed>
					<DefaultHeader onLogout={e => this.signOut(e)} />
				</AppHeader>
				<div className="app-body">
					<AppSidebar fixed display="lg">
						<AppSidebarHeader />
						<AppSidebarForm />
						<Suspense>
							<AppSidebarNav navConfig={navigation} {...this.props} router={router} />
						</Suspense>
						<AppSidebarFooter />
						<AppSidebarMinimizer />
					</AppSidebar>
					<main className="main">
						<AppBreadcrumb appRoutes={routes} router={router} />
						<Container fluid>
							<Switch>
								{routes.map((route, idx) => {
									return route.component ? <Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => <route.component {...props} />} /> : null;
								})}
								<Redirect from="/" to="/dashboard" />
							</Switch>
						</Container>
					</main>
					<AppAside fixed>
						<DefaultAside />
					</AppAside>
				</div>
				<AppFooter>
					<DefaultFooter />
				</AppFooter>
			</div>
		);
	}
}

export default DefaultLayout;
