import React, { Component } from 'react';
import { ClipLoader } from 'react-spinners';

class Loader extends Component {
	render() {
		return (
			<div className="siteLoader">
				<div className="loader-wrap">
					<div className="loader-wrapper">
						<div className="sweet-loading">
							<ClipLoader size={20} color={'#59c9e7'} loading={true} />
						</div>
					</div>
					<div>
						<span>Processing...</span>
					</div>
				</div>
			</div>
		);
	}
}

export default Loader;
