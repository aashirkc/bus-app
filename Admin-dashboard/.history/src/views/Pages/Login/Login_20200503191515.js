import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Alert, Col, Container, Form, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import auth from '../../../services/authService';
import { Formik, Field, ErrorMessage } from 'formik';
import loginSchema from '../../../utils/yupValidations/loginValidation';

class Login extends Component {
  state = {
    LoginErrors: {},
    data: {
      userName: '',
      password: '',
    },
  };

  render() {
    const { LoginErrors } = this.state;

    if (auth.getCurrentUser()) return <Redirect to="/dashboard" />;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    {LoginErrors && LoginErrors.message && <Alert color="danger">{LoginErrors.message.message}</Alert>}
                    <Formik
                      initialValues={this.state.data}
                      validationSchema={loginSchema}
                      onSubmit={async (values, actions) => {
                        actions.setSubmitting(true);

                        try {
                          // await auth.login(values);
                          // actions.setSubmitting(false);
                          // const { state } = this.props.location;
                          window.location.hash = state ? state.from.pathname : '/dashboard';
                        } catch (err) {
                          if (err.response && err.response.status === 400) {
                            const errors = { ...this.state.errors };
                            errors.message = err.response.data;
                            this.setState({ LoginErrors: errors });
                          } else {
                            this.setState({ LoginErrors: err });
                          }
                          actions.setSubmitting(false);
                        }
                      }}
                    >
                      {({ isSubmitting, handleSubmit, errors }) => (
                        <Form>
                          <h1>Login</h1>
                          <p className="text-muted">Sign In to your account</p>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-user"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field type="text" name="userName" autoComplete="off" className={errors.userName ? 'form-control is-invalid' : 'form-control'} placeholder={'Username'} />
                            {errors.userName && <ErrorMessage name="userName" component="div" className="invalid-feedback" />}
                          </InputGroup>
                          <InputGroup className="mb-4">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-lock"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field type="password" name="password" autoComplete="off" className={errors.password ? 'form-control is-invalid' : 'form-control'} placeholder={'Password'} />
                            {errors.password && <ErrorMessage name="password" component="div" className="invalid-feedback" />}
                          </InputGroup>
                          <Row>
                            <Col xs="6">
                              <Button onClick={handleSubmit} type="submit" color="primary" className="px-4">
                                Login
															</Button>
                            </Col>
                            <Col xs="6" className="text-right">
                              <Button color="link" className="px-0">
                                Forgot password?
															</Button>
                            </Col>
                          </Row>
                        </Form>
                      )}
                    </Formik>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
