import React, { Component } from 'react';
import { getSeat } from '../../services/seatService';
import { getSeatData } from '../../utils/table/seatDataTable';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';


class Seats extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: {},
      seatData: [],
      errors: {},
      SeatErrors: {},
      loading: true,
      dataResponse: false,
    }
  }



  async ComponentDidMount() {
    try {
      const { data: seatData } = await getSeat()
      this.setState({ seatData, loading: false });
    } catch (err) {
      this.setState({ errors: err });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: !prevState.loading });
    }
  }


  getBadge = status => {
    return status === 'true' ? 'success' : 'warning';
  };

  activeClass = val => {
    let newClass = val === true ? 'badge-success' : 'badge-warning';
    return newClass;
  };

  render() {
    const { seatData, errors, loading } = this.state;

    const data = getSeatData(seatData, this.handleDelete);
    return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'User Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
  }


}

export default Seats;

