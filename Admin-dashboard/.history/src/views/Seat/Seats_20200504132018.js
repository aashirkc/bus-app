import React, { Component } from 'react';
import { getSeat } from '../../services/seatService';



class Seats extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: {},
      seatData: [],
      errors: {},
      SeatErrors: {},
      loading: true,
      dataResponse: false,
    }
  }



  async ComponentDidMount() {
    try {
      const { data: seatData } = await getSeat()
      this.setState({ seatData, loading: false });
    } catch (err) {
      this.setState({ errors: err });
    }
  }


}

export default Seats;

