import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Table, Alert, Button } from 'reactstrap';
import { Formik, Field, ErrorMessage, getIn } from 'formik';
import { bookedOptions } from '../../utils/options/Options';
import { addSeat } from '../../services/seatService';
import { toast } from 'react-toastify';
import addUserSchema from '../../utils/yupValidations/addUserValidations';
// import { addUser } from '../../services/addUserServices';
// import { toast } from 'react-toastify';
import Loader from '../../common/loader';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
// import { getDocUrl } from '../../services/httpService';

class AddSeat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      errors: {},
      data: {
        busType: '',
        col1: [],
        col2: [],
        col3: [],
      },
      images: '',
      AddSeatErrors: {},
      rows: [],
      rows1: [],
      rows2: [],
      dataResponse: false,
    };
  }

  componentDidMount() {
    try {
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ errors: err });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: false });
    }
  }

  handleRow = setFieldValue => {
    console.log(setFieldValue)
    let { rows } = this.state;
    rows.length < 1 ? rows.push(0) : rows.push(rows.length);
    let rw = rows.length - 1;

    setFieldValue(`col1.${rw}.seatName`, '');
    setFieldValue(`col1.${rw}.isAvailable`, 'true');
    setFieldValue(`col1.${rw}.isBooked`, 'true');
    this.setState({ rows });
  };

  handleDeleteRow = () => {
    const { rows } = this.state;
    rows.splice(-1, 1);
    this.setState({ rows });
  };

  handleRow1 = setFieldValue => {
    let { rows1 } = this.state;
    rows1.length < 1 ? rows1.push(0) : rows1.push(rows1.length);
    let rw = rows1.length - 1;

    setFieldValue(`col2.${rw}.seatName`, '');
    setFieldValue(`col2.${rw}.isAvailable`, 'true');
    setFieldValue(`col2.${rw}.isBooked`, 'true');
    this.setState({ rows1 });
  };

  handleDeleteRow1 = () => {
    const { rows1 } = this.state;
    rows1.splice(-1, 1);
    this.setState({ rows1 });
  };

  handleRow2 = setFieldValue => {
    let { rows2 } = this.state;
    rows2.length < 1 ? rows2.push(0) : rows2.push(rows2.length);
    let rw = rows2.length - 1;

    setFieldValue(`col3.${rw}.seatName`, '');
    setFieldValue(`col3.${rw}.isAvailable`, 'true');
    setFieldValue(`col3.${rw}.isBooked`, 'true');
    this.setState({ rows2 });
  };

  handleDeleteRow2 = () => {
    const { rows2 } = this.state;
    rows2.splice(-1, 1);
    this.setState({ rows2 });
  };

  render() {
    const { AddSeatErrors, errors, loading, rows, rows1, rows2 } = this.state;
    return (
      <>
        {errors && errors.message ? (
          <NetworkErrorAlert />
        ) : loading ? (
          <Loader />
        ) : (
              <div className="animated fadeIn">
                {AddSeatErrors && AddSeatErrors.message && <Alert color="danger">{AddSeatErrors.message.message}</Alert>}
                <Formik
                  initialValues={this.state.data}

                  onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    console.log(values);

                    try {
                      const response = await addSeat(values)
                      if (response.data.error) throw new Error(response.data.error);
                      else {
                        toast.success('Added successfully!');
                        this.setState({ dataResponse: true, AddSeatErrors: {}, loading: true });
                      }
                      actions.resetForm();
                      actions.setSubmitting(false);
                    } catch (ex) {
                      if (ex.response && ex.response.status === 404) {
                        toast.error('Something went wrong.');
                      } else {
                        this.setState({ AddSeatErrors: ex });
                      }
                      actions.setSubmitting(false);
                    }

                    // // values.status = JSON.parse(values.status);

                    // const formData = new FormData();
                    // formData.append('fullName', values.fullName);
                    // formData.append('userName', values.userName);
                    // formData.append('email', values.email);
                    // formData.append('password', values.password);
                    // formData.append('dob', values.dob);
                    // formData.append('address', values.address);
                    // formData.append('role', values.role);
                    // formData.append('status', values.status);
                    // formData.append('phoneNo', values.phoneNo);

                    // // let fileNames = '';
                    // // const splitFilename = values.image[0];
                    // // if (splitFilename.length > 0) {
                    // // 	fileNames = splitFilename[splitFilename.length - 1];
                    // // }

                    // formData.append('image', values.image);

                    // delete values.confirmPassword;

                    // try {
                    // 	const response = await addUser(formData);
                    // 	if (response.data.error) throw new Error(response.data.error);
                    // 	else {
                    // 		toast.success('Added successfully!');
                    // 		this.setState({ dataResponse: true, AddUserErrors: {}, loading: true });
                    // 	}
                    // 	actions.resetForm();
                    // 	actions.setSubmitting(false);
                    // } catch (ex) {
                    // 	if (ex.response && ex.response.status === 404) {
                    // 		toast.error('Something went wrong.');
                    // 	} else {
                    // 		this.setState({ AddUserErrors: ex });
                    // 	}
                    // 	actions.setSubmitting(false);
                    // }
                  }}
                >
                  {({ values, handleChange, handleSubmit, handleBlur, errors, setFieldValue }) => (
                    <Form>
                      <Row>
                        <Col>
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Add Seat</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <Row>
                                <Col md="4">
                                  <FormGroup>
                                    <Label htmlFor="busType">Seat Type</Label>
                                    <Field type="text" name="busType" className={errors.busType ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Name'} autoComplete="off" />
                                    {errors.busType && <ErrorMessage name="busType" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                              </Row>
                              <Row>
                                <Col md="8">
                                  <div className="pb-3">
                                    <strong className="mr-2 mb-2">Column 1</strong>
                                    <Table bordered responsive size="sm">
                                      <thead>
                                        <tr>
                                          <th style={{ width: 10 }} />
                                          <th style={{ width: 200, textAlign: 'center' }}>Seat Name</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Available</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Booked</th>
                                          <th style={{ width: 40 }} />
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {rows &&
                                          rows.map((rw, idx) => (
                                            <tr key={idx}>
                                              <td>{idx + 1}</td>
                                              <td>
                                                <Field type="text" name={`col1.${rw}.seatName`} className={'form-control newTable-input'} placeholder={'Seat Name'} autoComplete="off" />
                                              </td>
                                              <td>
                                                <select name={`col1.${rw}.isAvailable`} value={values.isAvailable} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(avai => (
                                                    <option key={avai.id} value={avai.value} label={avai.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <select name={`col1.${rw}.isBooked`} value={values.isBooked} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(book => (
                                                    <option key={book.id} value={book.value} label={book.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <Button
                                                  size="sm"
                                                  color="danger"
                                                  active
                                                  onClick={e => {
                                                    this.handleDeleteRow();
                                                    setFieldValue('col1', values.col1 && values.col1.filter(el => el !== getIn(values, `col1.${rw}`)));
                                                  }}
                                                  type="button"
                                                  className="mt-1"
                                                >
                                                  <i className="cui-trash icons"></i>
                                                </Button>
                                              </td>
                                            </tr>
                                          ))}
                                        <tr>
                                          <td>
                                            <Button size="sm" block color="primary" onClick={() => this.handleRow(setFieldValue)} type="button">
                                              <i className="fa fa-plus fa-md"></i>
                                            </Button>
                                          </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </div>
                                  <div className="pb-3">
                                    <strong className="mr-2 mb-2">Column 2</strong>
                                    <Table bordered responsive size="sm">
                                      <thead>
                                        <tr>
                                          <th style={{ width: 10 }} />
                                          <th style={{ width: 200, textAlign: 'center' }}>Seat Name</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Available</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Booked</th>
                                          <th style={{ width: 40 }} />
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {rows1 &&
                                          rows1.map((rw, idx) => (
                                            <tr key={idx}>
                                              <td>{idx + 1}</td>
                                              <td>
                                                <Field type="text" name={`col2.${rw}.seatName`} className={'form-control newTable-input'} placeholder={'Seat Name'} autoComplete="off" />
                                              </td>
                                              <td>
                                                <select name={`col2.${rw}.isAvailable`} value={values.isAvailable} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(avai => (
                                                    <option key={avai.id} value={avai.value} label={avai.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <select name={`col2.${rw}.isBooked`} value={values.isBooked} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(book => (
                                                    <option key={book.id} value={book.value} label={book.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <Button
                                                  size="sm"
                                                  color="danger"
                                                  active
                                                  onClick={e => {
                                                    this.handleDeleteRow1();
                                                    setFieldValue('col2', values.col2 && values.col2.filter(el => el !== getIn(values, `col2.${rw}`)));
                                                  }}
                                                  type="button"
                                                  className="mt-1"
                                                >
                                                  <i className="cui-trash icons"></i>
                                                </Button>
                                              </td>
                                            </tr>
                                          ))}
                                        <tr>
                                          <td>
                                            <Button size="sm" block color="primary" onClick={() => this.handleRow1(setFieldValue)} type="button">
                                              <i className="fa fa-plus fa-md"></i>
                                            </Button>
                                          </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </div>
                                  <div className="pb-3">
                                    <strong className="mr-2 mb-2">Column 3</strong>
                                    <Table bordered responsive size="sm">
                                      <thead>
                                        <tr>
                                          <th style={{ width: 10 }} />
                                          <th style={{ width: 200, textAlign: 'center' }}>Seat Name</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Available</th>
                                          <th style={{ width: 200, textAlign: 'center' }}>Booked</th>
                                          <th style={{ width: 40 }} />
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {rows2 &&
                                          rows2.map((rw, idx) => (
                                            <tr key={idx}>
                                              <td>{idx + 1}</td>
                                              <td>
                                                <Field type="text" name={`col3.${rw}.seatName`} className={'form-control newTable-input'} placeholder={'Seat Name'} autoComplete="off" />
                                              </td>
                                              <td>
                                                <select name={`col3.${rw}.isAvailable`} value={values.isAvailable} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(avai => (
                                                    <option key={avai.id} value={avai.value} label={avai.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <select name={`col3.${rw}.isBooked`} value={values.isBooked} onChange={handleChange} className={'form-control newTable-input'}>
                                                  {bookedOptions.map(book => (
                                                    <option key={book.id} value={book.value} label={book.label} />
                                                  ))}
                                                </select>
                                              </td>
                                              <td>
                                                <Button
                                                  size="sm"
                                                  color="danger"
                                                  active
                                                  onClick={e => {
                                                    this.handleDeleteRow2();
                                                    setFieldValue('col3', values.col3 && values.col3.filter(el => el !== getIn(values, `col3.${rw}`)));
                                                  }}
                                                  type="button"
                                                  className="mt-1"
                                                >
                                                  <i className="cui-trash icons"></i>
                                                </Button>
                                              </td>
                                            </tr>
                                          ))}
                                        <tr>
                                          <td>
                                            <Button size="sm" block color="primary" onClick={() => this.handleRow2(setFieldValue)} type="button">
                                              <i className="fa fa-plus fa-md"></i>
                                            </Button>
                                          </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                    </Table>
                                  </div>
                                </Col>
                              </Row>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button type="button" onClick={handleSubmit} color="primary" className="px-4 mb-3">
                            Edit Seat
                                              </Button>
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </div>
            )}
      </>
    );
  }
}

export default AddSeat;
