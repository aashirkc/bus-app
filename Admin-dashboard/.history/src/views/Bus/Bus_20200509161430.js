import React, { Component } from 'react';
import { getBus, deleteBus } from '../../services/busServices';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import Loader from '../../common/loader';
import { toast } from 'react-toastify';
import MUIDataTable from 'mui-datatables';
import { getBusData } from '../../utils/table/busDataTable';

class ManageBus extends Component {
  constructor(props) {
    super(props);
    console.log(this.props)

    this.state = {
      data: {},
      BusData: [],
      errors: {},
      UserErrors: {},
      loading: true,
      dataResponse: false,
    };
  }

  async componentDidMount() {
    try {

      const { data: BusData } = await getBus();
      console.log(this.state.BusData)
      this.setState({ BusData, loading: false });
    } catch (err) {
      this.setState({ errors: err });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: !prevState.loading });
    }
  }

  handleDelete = async bus => {
    const originalbus = this.state.BusData;
    const BusData = originalbus.filter(m => m._id !== bus._id);
    this.setState({ BusData });

    try {
      await deleteBus(bus._id);
      toast.success('Bus deleted successfully.');
    } catch (ex) {
      if (ex.response && ex.response.status === 404) toast.error('This Bus has already been deleted.');
      this.setState({ BusData: originalbus, dataResponse: false, loading: true });
    }
  };

  render() {
    const { errors, loading } = this.state;

    const data = getBusData([], this.handleDelete);
    return <>{errors && errors.message ? <NetworkErrorAlert /> : loading ? <Loader /> : <MUIDataTable title={'Bus Data Table'} data={data.rows} columns={data.columns} options={data.options} />}</>;
  }
}

export default ManageBus;
