import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Alert, Button } from 'reactstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { busTypeOptions, activeOptions, busFeatureOptions } from '../../utils/options/Options';
import { addBus } from '../../services/busServices';
import { toast } from 'react-toastify';
import Loader from '../../common/loader';
import { getSeat } from '../../services/seatService';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      errors: {},
      data: {
        busName: '',
        busNo: '',
        busType: '',
        driverName: '',
        status: '',
        seatType: '',
        features: [],
        images: '',
        file: '',
      },
      AddBusErrors: {},
      checkedArray: [],

      dataResponse: false,
    };
  }

  async componentDidMount() {
    try {
      const { data: seatData } = await getSeat()
      this.setState(prevState => ({ seatData, loading: !prevState.loading }));

    } catch (err) {
      this.setState({ errors: err });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.state.dataResponse !== prevState.dataResponse) {
      this.setState({ loading: false });
    }
  }

  handleSeatChange = val => {
    // const { features } = this.state.data;
    this.setState(prevstate => {
      const prevCheckedArray = prevstate.data.features;
      let retCheckedArray;
      if (prevCheckedArray.find(rw => rw.id === val.id)) {
        retCheckedArray = prevCheckedArray.filter(row => row.id !== val.id);
      } else {
        retCheckedArray = prevCheckedArray.concat(val);
      }
      return {
        data: { ...this.state.data, features: retCheckedArray },
      };
    });
  };

  render() {
    const { seatData, AddBusErrors, errors, loading } = this.state;
    return (
      <>
        {errors && errors.message ? (
          <NetworkErrorAlert />
        ) : loading ? (
          <Loader />
        ) : (
              <div className="animated fadeIn">
                {AddBusErrors && AddBusErrors.message && <Alert color="danger">{AddBusErrors.message.message}</Alert>}
                <Formik
                  initialValues={this.state.data}
                  onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    console.log(values);
                    return;

                    values.features = this.state.data.features;
                    values.status = JSON.parse(values.status);
                    values.seatType = JSON.parse(values.seatType);

                    const formData = new FormData();
                    formData.append('busName', values.busName);
                    formData.append('busNo', values.busNo);
                    formData.append('busType', values.busType);
                    formData.append('driverName', values.driverName);
                    formData.append('status', values.status);
                    formData.append('seatType', values.seatType);
                    formData.append('features', values.features);
                    formData.append('images', values.images);

                    try {
                      const response = await addBus(formData);
                      if (response.data.error) throw new Error(response.data.error);
                      else {
                        toast.success('Added successfully!');
                        this.setState({ dataResponse: true, AddBusErrors: {}, loading: true });
                      }
                      actions.resetForm();
                      actions.setSubmitting(false);
                    } catch (ex) {
                      if (ex.response && ex.response.status === 404) {
                        toast.error('Something went wrong.');
                      } else {
                        this.setState({ AddBusErrors: ex });
                      }
                      actions.setSubmitting(false);
                    }
                  }}
                >
                  {({ values, handleChange, handleSubmit, handleBlur, errors, setFieldValue }) => (
                    <Form>
                      <Row>
                        <Col lg="9">
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Detail</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <Row>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busName">Bus Name</Label>
                                    <Field type="text" name="busName" className={errors.busName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Name'} autoComplete="off" />
                                    {errors.busName && <ErrorMessage name="busName" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busNo">Bus Number</Label>
                                    <Field type="text" name="busNo" className={errors.busNo ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Bus Number'} autoComplete="off" />
                                    {errors.busNo && <ErrorMessage name="busNo" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="busType">Bus Type</Label>
                                    <select name="busType" value={values.busType} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select busType'} value="" defaultChecked />
                                      {busTypeOptions.map(user => (
                                        <option key={user.id} value={user.value} label={user.label} />
                                      ))}
                                    </select>
                                    {errors.busType && <span className="invalid-feedback">{errors.busType}</span>}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="driverName">Driver Name</Label>
                                    <Field
                                      type="text"
                                      name="driverName"
                                      className={errors.driverName ? 'form-control is-invalid' : 'form-control'}
                                      placeholder={"Enter Driver's Name"}
                                      autoComplete="off"
                                    />
                                    {errors.driverName && <ErrorMessage name="driverName" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="status" className="mr-2">
                                      Status
																</Label>
                                    <select name="status" value={values.status} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select Status'} value={''} defaultChecked />
                                      {activeOptions.map(status => (
                                        <option key={status.id} value={status.value} label={status.label} />
                                      ))}
                                    </select>
                                    {errors.status && <span className="invalid-feedback">{errors.status}</span>}
                                  </FormGroup>
                                </Col>
                                <Col md="6">
                                  <FormGroup>
                                    <Label htmlFor="status" className="mr-2">
                                      Seat Type
																</Label>
                                    <select name="seatType" value={values.seatType} onChange={handleChange} className={'form-control'}>
                                      <option label={'Select Seat Type'} value={''} defaultChecked />
                                      {
                                        seatData.map(
                                          ta => <option key={ta.id} value={ta.id} label={ta.busType}></option>
                                        )
                                      }
                                    </select>
                                    {errors.seatType && <span className="invalid-feedback">{errors.seatType}</span>}
                                  </FormGroup>
                                </Col>
                              </Row>
                            </CardBody>
                          </Card>
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Features</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <Col lg="6" md="6">
                                <FormGroup>
                                  <FormGroup>
                                    <Label htmlFor="images">Select Image</Label>
                                    <input
                                      type="file"
                                      name="images"
                                      onBlur={handleBlur}
                                      multiple
                                      onChange={e => {
                                        setFieldValue('images', e.target.files);

                                        const arrFiles = Array.from(e.target.files);
                                        const multipleFiles = arrFiles.map((file, index) => {
                                          const src = window.URL.createObjectURL(file);
                                          return { file, id: index, src };
                                        });

                                        setFieldValue('file', multipleFiles);
                                      }}
                                      className={errors.images ? 'form-control is-invalid' : 'form-control'}
                                    />
                                    {errors.images && <ErrorMessage name="images" component="div" className="invalid-feedback" />}
                                  </FormGroup>
                                </FormGroup>
                              </Col>
                              <Col lg="12" md="12">
                                <FormGroup>
                                  <div className="bus_image--wrapper">
                                    {values.file &&
                                      values.file.map((value, index) => (
                                        <div className="bus_image" key={index}>
                                          <img src={value.src} alt="BusImages" />
                                        </div>
                                      ))}
                                  </div>
                                </FormGroup>
                              </Col>
                            </CardBody>
                          </Card>
                        </Col>
                        <Col lg="3">
                          <Card>
                            <CardHeader>
                              <strong className="mr-2">Bus Features</strong>
                              <small>Form</small>
                            </CardHeader>
                            <CardBody>
                              <FormGroup>
                                <Col md="12">
                                  {busFeatureOptions.map(m => (
                                    <FormGroup check className="checkbox pb-3" key={m.id}>
                                      <Field
                                        name="features"
                                        onChange={() => this.handleSeatChange(m)}
                                        className={errors.features ? 'form-check-input is-invalid' : 'form-check-input'}
                                        type="checkbox"
                                        id={m.id}
                                        value={values.features}
                                      />
                                      <Label check className="form-check-label" htmlFor={m.id}>
                                        {m.label}
                                      </Label>
                                    </FormGroup>
                                  ))}
                                  {errors.features && <ErrorMessage name="features" component="div" className="invalid-feedback" />}
                                </Col>
                              </FormGroup>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button type="button" onClick={handleSubmit} color="primary" className="px-4 mb-3">
                            Add Bus
											</Button>
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </div>
            )}
      </>
    );
  }
}

export default User;
