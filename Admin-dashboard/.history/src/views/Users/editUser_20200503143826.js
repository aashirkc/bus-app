import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Label, Form, Button, Alert } from 'reactstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { userOptions, activeOptions } from '../../utils/options/Options';
import { editUser, getUser } from '../../services/addUserServices';
import { toast } from 'react-toastify';
import Loader from '../../common/loader';
import NetworkErrorAlert from '../../common/networkErrorAlertMessage';
import { getDocUrl } from '../../services/httpService';
import editUserSchema from '../../utils/yupValidations/editUserValiation';

class EditUser extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			errors: {},
			data: {
				id: '',
				fullName: '',
				userName: '',
				email: '',
				password: '',
				confirmPassword: '',
				dob: '',
				address: '',
				role: '',
				status: '',
				phoneNo: '',
				image: [],
				photo: '',
				file: [],
			},
			images: '',
			AddUserErrors: {},
			dataResponse: false,
			isedit: false,
		};
	}

	async editUser() {
		try {
			const id = this.props.match.params.id;
			if (!id) {
				this.setState({ loading: false });
				return;
			}

			const { data: userdata } = await getUser(id);

			this.setState({ data: this.mapToViewModal(userdata), loading: false });
		} catch (ex) {
			if (ex.response && ex.response.status === 404) return this.props.history.replace('/404');

			this.setState({ errors: ex, loading: false, isedit: true });
		}
	}

	async componentDidMount() {
		await this.editUser();
	}

	async componentDidUpdate(prevProps, prevState) {
		if (this.state.dataResponse !== prevState.dataResponse) {
			await this.editUser();
			this.setState({ loading: false });
		}
	}

	mapToViewModal(user) {
		return {
			id: user._id,
			fullName: user.fullName || '',
			userName: user.userName || '',
			email: user.email || '',
			password: '' || '',
			confirmPassword: '' || '',
			dob: user.dob || '',
			address: user.address || '',
			role: user.role || '',
			status: user.status || '',
			phoneNo: user.phoneNo || '',
			image: user.image || '',
			file: user.file || '',
		};
	}

	render() {
		const { AddUserErrors, errors, loading, isedit } = this.state;

		console.log(`${getDocUrl()}/${this.state.data.image}`);

		return (
			<>
				{errors && errors.message ? (
					<NetworkErrorAlert />
				) : loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn">
						<Row>
							<Col xs="12">
								<Card>
									<CardHeader>
										<strong className="mr-2">Edit User</strong>
										<small>Form</small>
									</CardHeader>
									<CardBody>
										{AddUserErrors && AddUserErrors.message && <Alert color="danger">{AddUserErrors.message}</Alert>}
										<Formik
											initialValues={this.state.data}
											validationSchema={editUserSchema}
											onSubmit={async (values, actions) => {
												actions.setSubmitting(true);

												values.status = values.status === '' ? false : values.status;
												const formData = new FormData();
												formData.append('fullName', values.fullName);
												formData.append('userName', values.userName);
												formData.append('email', values.email);
												formData.append('password', values.password);
												formData.append('dob', values.dob);
												formData.append('address', values.address);
												formData.append('role', values.role);
												formData.append('status', values.status);
												formData.append('phoneNo', values.phoneNo);

												formData.append('image', values.image);

												try {
													const response = await editUser(values.id, formData);
													if (response.data.error) throw new Error(response.data.error);
													else {
														toast.success('Edited successfully!');
													}
													actions.resetForm();
													actions.setSubmitting(false);
													this.setState({ dataResponse: true, AddUserErrors: {}, loading: true });
												} catch (ex) {
													if (ex.response && ex.response.status === 404) {
														toast.error('Something went wrong.');
													} else {
														this.setState({ AddUserErrors: ex });
													}
													actions.setSubmitting(false);
												}
											}}
										>
											{({ values, handleChange, handleSubmit, handleBlur, errors, setFieldValue }) => (
												<Form onSubmit={handleSubmit}>
													<Row>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="fullName">Full Name</Label>
																<Field type="text" name="fullName" className={errors.fullName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Full Name'} autoComplete="off" />
																{errors.fullName && <ErrorMessage name="fullName" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="userName">Username</Label>
																<Field type="text" name="userName" className={errors.userName ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Username'} autoComplete="off" />
																{errors.userName && <ErrorMessage name="userName" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="email">Email</Label>
																<Field type="email" name="email" className={errors.email ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Email'} autoComplete="off" />
																{errors.email && <ErrorMessage name="email" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="phoneNo">Phone Number</Label>
																<Field type="number" name="phoneNo" className={errors.phoneNo ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Your Number'} autoComplete="off" />
																{errors.phoneNo && <ErrorMessage name="phoneNo" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="dob">Date of Birth</Label>
																<Field type="text" name="dob" className={errors.dob ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Date of Birth'} autoComplete="off" />
																{errors.dob && <ErrorMessage name="dob" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="address">Address</Label>
																<Field type="text" name="address" className={errors.address ? 'form-control is-invalid' : 'form-control'} placeholder={'Enter Address'} autoComplete="off" />
																{errors.address && <ErrorMessage name="address" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="role">Role</Label>
																<select name="role" value={values.role} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Role'} value="" defaultChecked />
																	{userOptions.map(user => (
																		<option key={user.id} value={user.value} label={user.label} />
																	))}
																</select>
																{errors.role && <span className="invalid-feedback">{errors.role}</span>}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="status" className="mr-2">
																	Status
																</Label>
																<select name="status" value={values.status} onChange={handleChange} className={'form-control'}>
																	<option label={'Select Status'} value={''} defaultChecked />
																	{activeOptions.map(status => (
																		<option key={status.id} value={status.value} label={status.label} />
																	))}
																</select>
																{errors.status && <span className="invalid-feedback">{errors.status}</span>}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															<FormGroup>
																<Label htmlFor="image">Image</Label>
																<input
																	type="file"
																	name="image"
																	onBlur={handleBlur}
																	onChange={e => {
																		setFieldValue('image', e.target.files[0]);

																		const src = window.URL.createObjectURL(e.target.files[0]);
																		setFieldValue('file', src);
																	}}
																	className={errors.image ? 'form-control is-invalid' : 'form-control'}
																/>
																{errors.image && <ErrorMessage name="image" component="div" className="invalid-feedback" />}
															</FormGroup>
														</Col>
														<Col lg="4" md="6">
															{!isedit && values.file ? (
																<div className="user-avatar">
																	<img src={values.file} alt="UserImage" />
																</div>
															) : (
																values.image && (
																	<div className="user-avatar">
																		<img src={`${getDocUrl()}/${values.image}`} alt="UserImage" />
																	</div>
																)
															)}
														</Col>
													</Row>
													<Row>
														<Col xs="6">
															<Button type="button" onClick={handleSubmit} color="primary" className="px-4 mt-3">
																Edit User
															</Button>
														</Col>
													</Row>
												</Form>
											)}
										</Formik>
									</CardBody>
								</Card>
							</Col>
						</Row>
					</div>
				)}
			</>
		);
	}
}

export default EditUser;
