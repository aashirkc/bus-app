import React from 'react';
import { getDocUrl } from '../../services/httpService';
import { Badge, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const activeClass = val => {
  let newClass = val === true ? 'badge-success' : 'badge-warning';
  return newClass;
};

const getBadge = status => {
  return status === 'true' ? 'success' : 'warning';
};

export const getSeatData = (data, handleDelete) => {
  const formData = data.map(row => {
    return {
      // name: (
      //   <div className="avatar">
      //     <img src={`${getDocUrl()}/${row.image}`} alt="admin" />
      //     <span className={`avatar-status ${activeClass(row.status)}`}></span>
      //   </div>
      // ),
      busType: row.busType || <span>-</span>,
      // email: row.email || <span>-</span>,
      // role: row.role || <span>-</span>,
      // status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
      actions: (
        <>
          <Link to={`/seats/${row._id}`}>
            <Button size="sm" className="btn-twitter btn-brand mr-1">
              <i className="fa fa-edit"></i>
            </Button>
          </Link>
          <Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
            <i className="fa fa-remove"></i>
          </Button>
        </>
      ),
    };
  });

  const Options = {
    print: false,
    download: false,
    viewColumns: false,
    filterTable: false,
    filterType: false,
  };

  const returnData = {
    columns: [

      {
        name: 'busType',
        label: 'Bus Type',
      },
      // {
      //   name: 'col1',
      //   label: 'col1',

      // },
      // {
      //   name: 'col2',
      //   label: 'col2',
      // },
      // {
      //   name: 'col3',
      //   label: 'col3',
      // },
      {
        name: 'actions',
        label: 'Action',
        options: {
          filter: false,
          sort: false,
        },
      },
    ],
    rows: formData,
    options: Options,
  };
  return returnData;
};
