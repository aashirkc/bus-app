import React from 'react';
import { Badge, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const getBadge = status => {
	return status === 'true' ? 'success' : 'warning';
};

export const getBusData = (data, handleDelete) => {
	const formData = data.map(row => {
		return {
			busName: row.busName || <span>-</span>,
			busNumber: row.busNumber || <span>-</span>,
			busType: row.busType || <span>-</span>,
			driverName: row.driverName || <span>-</span>,
			status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
			seatType: row.seatType || <span>-</span>,
			// features: [],
			// images: row. || <span>-</span>,
			actions: (
				<>
					<Link to={`/users/${row._id}`}>
						<Button size="sm" className="btn-twitter btn-brand mr-1">
							<i className="fa fa-edit"></i>
						</Button>
					</Link>
					<Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
						<i className="fa fa-remove"></i>
					</Button>
				</>
			),
		};
	});

	const Options = {
		print: false,
		download: false,
		viewColumns: false,
		filterTable: false,
		filterType: false,
	};

	const returnData = {
		columns: [
			{
				name: 'name',
				label: 'Name',
			},
			{
				name: 'busNumber',
				label: 'Number',
				options: {
					filter: false,
					sort: false,
				},
			},
			{
				name: 'driverName',
				label: 'Driver Name',
			},
			{
				name: 'status',
				label: 'Status',
			},
			{
				name: 'seatType',
				label: 'Seat Type',
			},
			{
				name: 'actions',
				label: 'Action',
				options: {
					filter: false,
					sort: false,
				},
			},
		],
		rows: formData,
		options: Options,
	};
	return returnData;
};
