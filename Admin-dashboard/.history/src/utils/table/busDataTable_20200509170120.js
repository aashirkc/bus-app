import React from 'react';
import { Badge, Button } from 'reactstrap';
import { getDocUrl } from '../../services/httpService';
import { Link } from 'react-router-dom';
import Popup from "reactjs-popup";

const getBadge = status => {
  return status === 'true' ? 'success' : 'warning';
};

const activeClass = val => {
  let newClass = val === true ? 'badge-success' : 'badge-warning';
  return newClass;
};

// const getBadge = status => {
//   return status === 'true' ? 'success' : 'warning';
// };


const showPopUp = (data) => {
  return (
    <Popup trigger={<button> Trigger</button>} position="right center">
      <div>Popup content here !!</div>
    </Popup>
  )
};


export const getBusData = (data, handleDelete) => {
  const formData = data.map(row => {
    return {
      name: (
        <div className="avatar">
          <img src={`${getDocUrl()}/${row.image}`} alt="admin" />
          <span className={`avatar-status ${activeClass(row.status)}`}></span>
        </div>
      ),
      busNo: row.busNo || <span>-</span>,
      busName: row.busName || <span>-</span>,
      busNumber: row.busNumber || <span>-</span>,
      busType: row.busType || <span>-</span>,
      driverName: row.driverName || <span>-</span>,
      seatType: row.seatType.busType || <span>-</span>,
      features: (
        <>
          <Popup trigger={<button> View</button>} position="right center">
            <div>Popup content here !!</div>
          </Popup>
        </>
      ),

      status: <Badge color={getBadge(`${row.status}`)}>{row.status === true ? 'Active' : 'Pending'}</Badge> || <span>-</span>,
      // seatType: row.seatType || <span>-</span>,
      // features: [],
      // images: row. || <span>-</span>,
      actions: (
        <>
          <Link to={`/users/${row._id}`}>
            <Button size="sm" className="btn-twitter btn-brand mr-1">
              <i className="fa fa-edit"></i>
            </Button>
          </Link>
          <Button size="sm" onClick={() => handleDelete(row)} className="btn-pinterest btn-brand mr-1">
            <i className="fa fa-remove"></i>
          </Button>
        </>
      ),
    };
  });

  const Options = {
    print: false,
    download: false,
    viewColumns: false,
    filterTable: false,
    filterType: false,
  };

  const returnData = {
    columns: [
      {
        name: 'name',
        label: 'Name',
      },
      {
        name: 'busNo',
        label: 'Number',
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: 'busName',
        label: 'Bus Name',
      },
      {
        name: 'busType',
        label: 'Bus Type',
      },
      {
        name: 'seatType',
        label: 'Seat Type',
      },

      {
        name: 'features',
        label: 'Features',
      },
      {
        name: 'driverName',
        label: 'Driver Name',
      },
      {
        name: 'status',
        label: 'Status',
      },

      {
        name: 'actions',
        label: 'Action',
        options: {
          filter: false,
          sort: false,
        },
      },
    ],
    rows: formData,
    options: Options,
  };
  return returnData;
};
