import http from './httpService';

const apiEndPoint = '/api/seat';


export function addSeat(value) {
  return http.post(`${apiEndPoint}`, value)
}
