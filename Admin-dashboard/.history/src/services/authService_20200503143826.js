import jwtDecode from 'jwt-decode';
import http from './httpService';
import { setLocalStorage, getLocalStorage } from '../utils/securedUtils/securedUtils';

const apiEndpoint = '/api/login';
const tokenKey = 'token';

export const login = async values => {
	const response = await http.post(`${apiEndpoint}`, values);
	if (response.data.error) throw new Error(response.data.error.message);

	const {
		data: { token: jwt },
	} = response;


	setLocalStorage(tokenKey, jwt);
};

export const logout = () => {
	localStorage.removeItem(tokenKey);
};

export const getCurrentUser = () => {
	try {
		const jwt = getLocalStorage(tokenKey);
		if (jwtDecode(jwt).exp < Date.now() / 1000) {
			logout();
		} else {
			return jwtDecode(jwt);
		}
	} catch (err) {
		return null;
	}
};

export const getJwt = () => {
	return getLocalStorage(tokenKey);
};

http.setJwt(getJwt());

export default {
	login,
	logout,
	getJwt,
	getCurrentUser,
};
