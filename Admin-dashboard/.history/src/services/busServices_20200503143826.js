import http from './httpService';

const apiEndpoint = '/api/bus';

export function addBus(value) {
	return http.post(`${apiEndpoint}`, value);
}

export function getBuses() {
	return http.get(`${apiEndpoint}`);
}

export function getBus(id) {
	return http.get(`${apiEndpoint}/${id}`);
}

export function editBus(id, value) {
	return http.put(`${apiEndpoint}/${id}`, value);
}

export function deleteBus(id) {
	return http.delete(`${apiEndpoint}/${id}`);
}
