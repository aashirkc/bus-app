import jwtDecode from 'jwt-decode';
import http from './httpService';
import { setLocalStorage, getLocalStorage } from '../utils/securedUtils/securedUtils';

const apiEndpoint = '/api/login';
const tokenKey = 'token';

export const login = async values => {
  const response = await http.post(`${apiEndpoint}`, values);
  if (response.data.error) throw new Error(response.data.error.message);
  console.log(response)

  const {
    data: { token: jwt },
  } = response;

  const { data: { user: user } } = response


  setLocalStorage(tokenKey, jwt);
  setLocalStorage("user", JSON.stringify(user))
};

export const logout = () => {
  localStorage.removeItem(tokenKey);
  localStorage.removeItem("user")
};


export const userData = () => {
  a = localStorage.getItem(JSON.parse(user))
  return a;
}

export const getCurrentUser = () => {
  try {
    // if (1 === 1) {
    //   return true
    // }
    const jwt = getLocalStorage(tokenKey);
    if (jwtDecode(jwt).exp < Date.now() / 1000) {
      logout();
    } else {
      return jwtDecode(jwt);
    }
  } catch (err) {
    return null;
  }
};

export const getJwt = () => {
  return getLocalStorage(tokenKey);
};

http.setJwt(getJwt());

export default {
  login,
  logout,
  getJwt,
  getCurrentUser,
  userData
};
