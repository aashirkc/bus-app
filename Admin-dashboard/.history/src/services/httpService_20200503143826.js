import axios from 'axios';

const REACT_APP_API_URL = 'http://192.168.100.131:3200';

axios.defaults.baseURL = REACT_APP_API_URL;

const setJwt = jwt => {
	axios.defaults.headers.common['Authorization'] = 'Bearer ' + jwt;
};

export const getDocUrl = () => {
	return REACT_APP_API_URL;
};

export default {
	get: axios.get,
	post: axios.post,
	put: axios.put,
	patch: axios.patch,
	delete: axios.delete,
	setJwt,
};
