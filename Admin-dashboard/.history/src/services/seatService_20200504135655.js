import http from './httpService';

const apiEndPoint = '/api/seat';


export function addSeat(value) {
  return http.post(`${apiEndPoint}`, value);
}


export function getSeat() {
  return http.get(`${apiEndPoint}`);
}
