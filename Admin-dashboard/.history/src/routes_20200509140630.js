import loadable from '@loadable/component';

const Breadcrumbs = loadable(() => import('./views/Base/Breadcrumbs'));
const Cards = loadable(() => import('./views/Base/Cards'));
const Carousels = loadable(() => import('./views/Base/Carousels'));
const Collapses = loadable(() => import('./views/Base/Collapses'));
const Dropdowns = loadable(() => import('./views/Base/Dropdowns'));
const Forms = loadable(() => import('./views/Base/Forms'));
const Jumbotrons = loadable(() => import('./views/Base/Jumbotrons'));
const ListGroups = loadable(() => import('./views/Base/ListGroups'));
const Navbars = loadable(() => import('./views/Base/Navbars'));
const Navs = loadable(() => import('./views/Base/Navs'));
const Paginations = loadable(() => import('./views/Base/Paginations'));
const Popovers = loadable(() => import('./views/Base/Popovers'));
const ProgressBar = loadable(() => import('./views/Base/ProgressBar'));
const Switches = loadable(() => import('./views/Base/Switches'));
const Tables = loadable(() => import('./views/Base/Tables'));
const Tabs = loadable(() => import('./views/Base/Tabs'));
const Tooltips = loadable(() => import('./views/Base/Tooltips'));
const BrandButtons = loadable(() => import('./views/Buttons/BrandButtons'));
const ButtonDropdowns = loadable(() => import('./views/Buttons/ButtonDropdowns'));
const ButtonGroups = loadable(() => import('./views/Buttons/ButtonGroups'));
const Buttons = loadable(() => import('./views/Buttons/Buttons'));
const Charts = loadable(() => import('./views/Charts'));
const Dashboard = loadable(() => import('./views/Dashboard'));
const CoreUIIcons = loadable(() => import('./views/Icons/CoreUIIcons'));
const Flags = loadable(() => import('./views/Icons/Flags'));
const FontAwesome = loadable(() => import('./views/Icons/FontAwesome'));
const SimpleLineIcons = loadable(() => import('./views/Icons/SimpleLineIcons'));
const Alerts = loadable(() => import('./views/Notifications/Alerts'));
const Badges = loadable(() => import('./views/Notifications/Badges'));
const Modals = loadable(() => import('./views/Notifications/Modals'));
const Colors = loadable(() => import('./views/Theme/Colors'));
const Typography = loadable(() => import('./views/Theme/Typography'));
const Widgets = loadable(() => import('./views/Widgets/Widgets'));
const ManageBus = loadable(() => import('./views/Bus/Bus'));
const AddBus = loadable(() => import('./views/Bus/AddBus'));
const EditBus = loadable(() => import('./views/Edit/EditBus'));
const Users = loadable(() => import('./views/Users/Users'));
const AddUser = loadable(() => import('./views/Users/AddUser'));
const EditUser = loadable(() => import('./views/Users/editUser'));
const ChangePassword = loadable(() => import('./views/Users/changepassword'));
const Seats = loadable(() => import('./views/Seat/Seats'));
const AddSeat = loadable(() => import('./views/Seat/AddSeat'));
const EditSeat = loadable(() => import('./views/Seat/EditSeat.js'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', exact: true, name: 'Theme', component: Colors },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', exact: true, name: 'Base', component: Cards },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/forms', name: 'Forms', component: Forms },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/charts', name: 'Charts', component: Charts },

  { path: '/bus', exact: true, name: 'Bus', component: ManageBus },
  { path: '/bus/add', exact: true, name: 'Add Bus', component: AddBus },
  { path: '/bus/:id', exact: true, name: 'Edit Bus', component: EditBus },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/add', name: 'Add User', component: AddUser },
  { path: '/users/:id', name: 'Edit User', component: EditUser },
  { path: '/changepassword', exact: true, name: 'Change Password', component: ChangePassword },
  { path: '/seats', exact: true, name: 'Seats', component: Seats },
  { path: '/seats/add', name: 'Add Seat', component: AddSeat },
  // { path: '/seats/edit', name: 'Add Seat', component: AddSeat },
  { path: '/seats/:id', name: 'Edit Seat', component: EditSeat },
];

export default routes;
