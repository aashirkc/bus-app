import React, { Component } from 'react';

import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg';
import sygnet from '../../assets/img/brand/sygnet.svg';
import authService from '../../services/authService';
import { getDocUrl } from '../../services/httpService';
import { inherits } from 'util';

const propTypes = {
  children: PropTypes.node,
};


const defaultProps = {};

class DefaultHeader extends Component {

  state = {
    images: ''
  }

  componentDidMount() {
    const image = authService.userData().image

    console.log(image)
    this.state.image = image
    console.log(this.state)
  }

  handleLogout = () => {
    authService.logout();
    window.location = '/'
  };

  render() {
    const style = {
      color: '#63c2de',
      "marginLeft": '6px',
      font: 'inherit',

    }
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <span style={style}><i className="fa fa-bus"></i> <b>Bus Ticketing System</b></span>
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-bell"></i>
              <Badge pill color="danger">
                5
							</Badge>
            </NavLink>
          </NavItem>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={`${getDocUrl}/${authService.userData.image}`} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Settings</strong>
              </DropdownItem>
              <DropdownItem>
                <Link to="/changepassword" className="nav-link">
                  <i className="fa fa-user"></i>Change Password
								</Link>
              </DropdownItem>
              <DropdownItem>
                <Link to="/users" className="nav-link">
                  <i className="fa fa-user"></i>Manage Users
								</Link>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-wrench"></i> Settings
							</DropdownItem>
              <DropdownItem>
                <Link to="/login" onClick={this.handleLogout} className="nav-link">
                  <i className="fa fa-lock"></i> Logout
								</Link>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
