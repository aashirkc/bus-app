const joi = require("joi");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busSchema = new mongoose.Schema({
  busName: {
    type: String,
    required: true,
    maxlength: 50
  },
  busNo: {
    type: String,
    required: true,
    maxlength: 50
  },
  busType: {
    type: String,
    required: true,
    maxlength: 40
  },
  seatType: {
    type: Schema.Types.ObjectId,
    ref: "BusSeat"
  },
  driverName: {
    type: String
  },

  features: {
    type: String,


  },
  status: {
    type: Boolean
  },
  image: {
    type: String
  }
});

const Bus = mongoose.model("Bus", busSchema);

function validateBus(bus) {
  const schema = {
    busNo: joi.string().required(),
    busType: joi.string().required(),
    busName: joi.allow(),
    seatType: joi.allow(),
    status: joi.allow(),
    features: joi.allow(),
    driverName: joi.allow(),

    image: joi.allow()
  };

  return joi.validate(bus, schema);
}

exports.Bus = Bus;
exports.busSchema = busSchema;
exports.validate = validateBus;
