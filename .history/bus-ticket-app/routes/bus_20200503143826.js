const { Bus, validate } = require("../model/bus");
const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();
const multer = require("multer");

const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/", auth, async (req, res) => {
  const bus = await Bus.find();
  res.send(bus);
});

router.post("/", upload.single("image"), async (req, res) => {
  console.log(req.body);
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const seat = [];
  Array.from(req.body);
  console.log(req.body);

  for (let i = 0; i < req.body.isSeat.length; i++) {
    seat[i] = {};
    seat[i].name = req.body.name[i];
    seat[i].isSeat = req.body.isSeat[i];
    seat[i].column = req.body.column[i];
    seat[i].isSeat = req.body.isSeat[i];
    seat[i].isBooked = req.body.isBooked[i];
  }

  // Seat.push(seat);

  console.log(seat);
  let bus = new Bus({
    busNo: req.body.busNo,
    busType: req.body.busType,
    busSeat: seat,
    isAvailable: req.body.isAvailable,
    image: req.file.path
  });

  bus = await bus.save();
  res.send(bus);
});

router.put("/:id", upload.single("image"), async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const bus = await Bus.findByIdAndUpdate(
    req.params.id,
    {
      busNo: req.body.busNo,
      busType: req.body.busType,
      noOfSeats: req.body.noOfSeats,
      noOfRows: req.body.noOfRows,
      noOfColumns: req.body.noOfColumns,
      isAvailable: req.body.isAvailable,
      image: req.file.path
    },
    { new: true }
  );

  if (!bus)
    res.status(400).send({ error: "The bus with given id is not found" });
  res.send(bus);
});

router.delete("/:id", async (req, res) => {
  let a = await Bus.findById(req.params.id);
  console.log(a.image);
  fs.unlink("./" + a.image, function(er) {
    if (er) throw er;
  });

  const bus = await Bus.findByIdAndRemove(req.params.id);

  if (!bus)
    return res
      .status(404)
      .send({ error: "The bus with the given ID was not found." });

  res.send(bus);
});

module.exports = router;
