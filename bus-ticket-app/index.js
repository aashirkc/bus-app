const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const bus = require("./routes/bus");
const user = require("./routes/user");
const routes = require("./routes/routes");
const seats = require("./routes/seat");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const auth = require("./routes/auth");

mongoose
  .connect("mongodb://localhost/bus_reservation", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log("Connected to Mongodb..."))
  .catch(err => console.error("could not connect to MongoDb"));

app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));
app.use("/api/bus", bus);
app.use("/uploads", express.static("uploads"));
app.use("/api/routes", routes);
app.use("/api/seat", seats);
app.use("/api/user", user);
app.use("/api/login", auth);
app.use("/api/seat", busSeat);

app.use(helmet());

app.listen(3200, "0.0.0.0", () => console.log("Listening to port 3200"));
