const { Bus, validate } = require("../model/bus");
const { BusSeat } = require('../model/busSeats');
const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();
const multer = require("multer");

const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/", auth, async (req, res) => {
  const bus = await Bus.find().populate('seatType');
  console.log(bus)

  res.send(bus);
});

router.get("/:id", async (req, res) => {
  let a = await Bus.findById(req.params.id).populate('seatType');
  res.send(a);
});


router.post("/", upload.single("image"), async (req, res) => {
  console.log(req.body);
  console.log(req.file)

  const { error } = validate(req.body);
  if (error) return res.status(400).send({ error: error.details[0].message });


  // Seat.push(seat);

  // console.log(seat);
  let bus = new Bus({
    busNo: req.body.busNo,
    busName: req.body.busName,
    busType: req.body.busType,
    seatType: req.body.seatType,
    status: req.body.status,
    features: req.body.features,
    driverName: req.body.driverName,
    image: req.file.path || ""
  });
  console.log(bus)
  try {
    console.log("sav1ing")
    bus = await bus.save();
  } catch (ex) {
    console.log("saving bus", ":", ex)
    res.send({
      error: ex
    })
  }
  res.send(bus);
});

router.put("/:id", upload.single("image"), async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const bus = await Bus.findByIdAndUpdate(
    req.params.id,
    {
      busNo: req.body.busNo,
      busName: req.body.busName,
      busType: req.body.busType,
      seatType: req.body.seatType,
      status: req.body.status,
      features: req.body.features,
      driverName: req.body.driverName,
      image: req.file.path || ""
    },
    { new: true }
  );

  if (!bus)
    res.status(400).send({ error: "The bus with given id is not found" });
  res.send(bus);
});

router.delete("/:id", async (req, res) => {
  let a = await Bus.findById(req.params.id);
  console.log(a.image);
  fs.unlink("./" + a.image, function (er) {
    if (er) throw er;
  });

  const bus = await Bus.findByIdAndRemove(req.params.id);

  if (!bus)
    return res
      .status(404)
      .send({ error: "The bus with the given ID was not found." });

  res.send(bus);
});

module.exports = router;
