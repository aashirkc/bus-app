const express = require("express");
const router = express.Router();
const { BusSeat, validate } = require("../model/busSeats")


router.get('/', async (req, res) => {
    const seat = await BusSeat.find();
    res.send(seat)
})


router.post('/', async (req, res) => {
    console.log(req.body)

    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);


    let seat = new BusSeat({
        busType: req.body.busType,
        col1: req.body.col1,
        col2: req.body.col1,
        col3: req.body.col3,

    });

    seat = await seat.save();
    res.send(seat);
})

router.delete("/:id", async (req, res) => {

    const bus = await BusSeat.findByIdAndRemove(req.params.id);

    if (!bus)
        return res
            .status(404)
            .send({ error: "The bus with the given ID was not found." });

    res.send(bus);
});

module.exports = router