const express = require("express");
const router = express.Router();
const { BusSeat, validate } = require("../model/busSeats")


router.get('/', async (req, res) => {
    const seat = await BusSeat.find();
    res.send(seat)
})


router.post('/', async (req, res) => {
    console.log(req)
})

module.exports = router