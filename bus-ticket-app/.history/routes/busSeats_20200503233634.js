const express = require("express");
const router = express.Router();
const { BusSeat, validate } = require("../model/busSeats")


router.get('/', async (req, res) => {
    const seat = await BusSeat.find();
    res.send(seat)
})

module.exports = router