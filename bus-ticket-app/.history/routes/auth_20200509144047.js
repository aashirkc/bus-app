const express = require("express");
const bcrypt = require("bcrypt");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const router = express.Router();
const { User } = require("../model/user");

router.post("/", async (req, res) => {
  const { error } = ValidateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ userName: req.body.userName });

  console.log(user);
  if (!user) return res.status(400).send({ message: "Invalid Username" });

  const validPassword = await bcrypt.compare(req.body.password, user.password);

  if (!validPassword)
    return res.status(400).send({ message: "Invalid Password" });


  const token = user.generateAuthToken();

  // const token = "21312312312312"

  res.header("x-auth-token", token).send({
    token: token,
    message: "Logged in sucessfully"
  });
});

function ValidateUser(user) {
  schema = {
    userName: joi.string().required(),

    password: joi
      .string()

      .required()
  };
  return joi.validate(user, schema);
}

module.exports = router;
