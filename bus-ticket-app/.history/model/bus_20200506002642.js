const joi = require("joi");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busSchema = new mongoose.Schema({
  busName: {
    type: String,
    required: true,
    maxlength: 50
  },
  busNo: {
    type: String,
    required: true,
    maxlength: 50
  },
  busType: {
    type: String,
    required: true,
    maxlength: 40
  },
  busSeat: {
    type: Schema.Types.ObjectId,
    ref: "BusSeat"
  },
  driverName: {
    type: String
  },
  features: {
    type: String,


  },
  status: {
    type: Boolean
  }
  images: {
    type: String
  }
});

const Bus = mongoose.model("Bus", busSchema);

function validateBus(bus) {
  const schema = {
    busNo: joi.string().required(),
    busType: joi.string().required(),
    busSeat: joi.allow(),
    name: joi.allow(),
    column: joi.allow(),
    isSeat: joi.allow(),
    isBooked: joi.allow(),
    isAvailable: joi.allow(),
    image: joi.allow()
  };

  return joi.validate(bus, schema);
}

exports.Bus = Bus;
exports.busSchema = busSchema;
exports.validate = validateBus;
