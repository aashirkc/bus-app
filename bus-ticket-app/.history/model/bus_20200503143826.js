const joi = require("joi");
const mongoose = require("mongoose");

const busSchema = new mongoose.Schema({
  busNo: {
    type: String,
    required: true,
    maxlength: 50
  },
  busType: {
    type: String,
    required: true,
    maxlength: 40
  },
  busSeat: [
    {
      type: new mongoose.Schema({
        name: {
          type: String,
          required: true,
          maxlength: 4,
          minlength: 2
        },
        column: {
          type: String
        },
        isSeat: {
          type: Boolean,
          default: false
        },
        isBooked: {
          type: Boolean,
          default: false
        }
      })
    }
  ],
  noOfColumns: {
    type: Number
  },
  isAvailable: {
    type: Boolean,

    default: true
  },
  image: {
    type: String
  }
});

const Bus = mongoose.model("Bus", busSchema);

function validateBus(bus) {
  const schema = {
    busNo: joi.string().required(),
    busType: joi.string().required(),
    busSeat: joi.allow(),
    name: joi.allow(),
    column: joi.allow(),
    isSeat: joi.allow(),
    isBooked: joi.allow(),
    isAvailable: joi.allow(),
    image: joi.allow()
  };

  return joi.validate(bus, schema);
}

exports.Bus = Bus;
exports.busSchema = busSchema;
exports.validate = validateBus;
