const mongoose = require("mongoose");
const joi = require("joi");

const busSeat = new mongoose.Schema({
  seatName: {
    reuired: true,
    maxlength: 4,
    minlength: 2
  },
  col1: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })

  }],
  col2: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })
  }],
  col3: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })
  }]


});

const BusSeat = new mongoose.model("BusSeat", busSeat);

// function validateBusSeat(busSeat) {
//     const schema = {
//       busNo: joi.string().required(),
//       busType: joi.string().required(),
//       noOfSeats: joi.allow(),
//       noOfRows: joi.allow(),
//       noOfColumns: joi.allow(),
//       isAvailable: joi.allow(),
//       image: joi.allow()
//     };

//     return joi.validate(bus, schema);
//   }

exports.BusSeat = BusSeat;
exports.busSeat = busSeat;
