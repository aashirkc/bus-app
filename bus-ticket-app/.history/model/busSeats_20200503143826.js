const mongoose = require("mongoose");
const joi = require("joi");

const busSeat = new mongoose.Schema({
  name: {
    reuired: true,
    maxlength: 4,
    minlength: 2
  },
  column: {
    type: Number
  },
  isAvailable: {
    type: Boolean,
    default: false
  },
  isBooked: {
    type: Boolean,
    default: false
  }
});

const BusSeat = new mongoose.model("BusSeat", busSeat);

// function validateBusSeat(busSeat) {
//     const schema = {
//       busNo: joi.string().required(),
//       busType: joi.string().required(),
//       noOfSeats: joi.allow(),
//       noOfRows: joi.allow(),
//       noOfColumns: joi.allow(),
//       isAvailable: joi.allow(),
//       image: joi.allow()
//     };

//     return joi.validate(bus, schema);
//   }

exports.BusSeat = BusSeat;
exports.busSeat = busSeat;
