const mongoose = require("mongoose");
const joi = require("joi");

const busSeat = new mongoose.Schema({
  BusType: {
    reuired: true,
    maxlength: 4,
    minlength: 2
  },
  col1: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })

  }],
  col2: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })
  }],
  col3: [{
    type: new mongoose.Schema({
      seatName: {
        required: true
      },
      isAvailable: {
        type: Boolean,
        default: false
      },
      isBooked: {
        type: Boolean,
        default: false
      }
    })
  }]


});

const BusSeat = new mongoose.model("BusSeat", busSeat);

function validateBusSeat(busSeat) {
  const schema = {

    busType: joi.string().required(),
    col1: joi.allow(),
    col2: joi.allow(),
    col3: joi.allow(),

  };

  return joi.validate(bus, schema);
}

exports.BusSeat = BusSeat;
exports.busSeat = busSeat;
