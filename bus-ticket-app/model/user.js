const mongoose = require("mongoose");
const joi = require("joi");
const jwt = require("jsonwebtoken");

const user = new mongoose.Schema({
  fullName: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 100
  },

  phoneNo: {
    type: Number
  },
  address: {
    type: String
  },
  dob: {
    type: String
  },
  userName: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  confirmPassword: {
    type: String
  },
  role: {
    type: String
  },
  status: {
    type: Boolean
  },
  image: {
    type: String
  }
});
user.methods.generateAuthToken = () => {
  const token = jwt.sign(
    { _id: this._id, userName: this.userName, role: this.role },
    "jwtPrivateKey"
  );
  return token;
};
const User = mongoose.model("User", user);
function validateUser(user) {
  const schema = {
    fullName: joi.string().required(),
    address: joi.allow(),
    dob: joi.allow(),
    userName: joi.allow(),
    email: joi.allow(),
    phoneNo: joi.allow(),
    role: joi.allow(),
    status: joi.allow(),
    password: joi.allow(),
    confirmPassword: joi.allow(),
    image: joi.allow()
  };

  return joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;
