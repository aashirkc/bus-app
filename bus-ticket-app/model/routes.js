const mongoose = require('mongoose');

const schema = mongoose.Schema(
	{
		routeName: {
			type: String,
			required: false,
		},
		startDistrict:{
			type: String,
			required: false,
		},
		endDistrict:{
			type: String,
			required: false,
		},
     		
	},
	{
		collection: 'routes',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
);

module.exports = mongoose.model('Routes', schema);