const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busSchema = new mongoose.Schema({
  busName: {
    type: String,
    required: true,
    maxlength: 50
  },
  busNumber: {
    type: String,
    required: true,
    maxlength: 40
  },
  seatType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Seats",
    required: true
  },
  driverName: {
    type: String,
    required: true,
    maxlength: 50
  },
  driverNumber: {
    type: String,
    required: true,
    maxlength: 40
  },
  startPlace:{
    type: String,
    required: true
  },
  departureDay:{
    type: String,
    required: true
  },
  destinationPlace:{
    type: String,
    required: true
  },
  arrivalDay:{
    type: String,
    required: true
  },
  type:{
     type: String,
    required: true
  },
  routeName: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Routes",
    required: true
  },
  features: {
     type: String,
    required: true,
  },
  status: {
    type: Boolean,
    required: true,
  },

  images: {
    data: Buffer,
    contentType: String
  }},
  {
		collection: 'buses',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	});


module.exports = mongoose.model('Bus', busSchema);
