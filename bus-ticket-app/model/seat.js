const mongoose = require('mongoose');

const schema = mongoose.Schema(
	{
		seatType: {
            type:String,
            required: true
        },
        col1:[{
            isAvailable:{
                type:Boolean
            },
             isBooked:{
                type:Boolean
            },
             seatName:{
                type:String
            }
        }],
        col2:[{
            isAvailable:{
                type:Boolean
            },
             isBooked:{
                type:Boolean
            },
             seatName:{
                type:String
            }
        }],
        col3:[{
            isAvailable:{
                type:Boolean
            },
             isBooked:{
                type:Boolean
            },
             seatName:{
                type:String
            }
        }]	
	},
	{
		collection: 'seats',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
);

module.exports = mongoose.model('Seats', schema);