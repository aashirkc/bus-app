const { User, validate } = require("../model/user");
const bcrypt = require("bcrypt");
const express = require("express");
const router = express.Router();
const multer = require("multer");
const fs = require("fs");

const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/", async (req, res) => {
  const user = await User.find();
  res.send(user);
});

router.get("/:id", async (req, res) => {
  let a = await User.findById(req.params.id);
  res.send(a);
});

router.post("/", upload.single("image"), async (req, res) => {
  console.log(req.body.phoneNo);

  try {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let email = await User.findOne({ email: req.body.email });
    if (email)
      return res.status(400).send({ message: "Email already registered." });

    let userName = await User.findOne({ userName: req.body.userName });
    if (userName)
      return res.status(400).send({ message: "UserName already used" });

    if (req.file) {
      let user = new User({
        fullName: req.body.fullName,
        phoneNo: req.body.phoneNo,
        userName: req.body.userName,
        dob: req.body.dob,
        address: req.body.address,
        email: req.body.email,
        role: req.body.role,
        status: req.body.status,
        password: req.body.password,
        image: req.file.path
      });
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(user.password, salt);

      user = await user.save();
      res.send({ message: "sucesss" });
    } else {
      let user = new User({
        fullName: req.body.fullName,
        phoneNo: req.body.phoneNo,
        userName: req.body.userName,
        dob: req.body.dob,
        address: req.body.address,
        email: req.body.email,
        role: req.body.role,
        status: req.body.status,
        password: req.body.password,
        image: ""
      });
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(user.password, salt);

      user = await user.save();
      res.send({ message: "sucesss" });
    }

    // let user = new User({
    //   fullName: req.body.fullName,
    //   phoneNo: req.body.phoneNo,
    //   userName: req.body.userName,
    //   dob: req.body.dob,
    //   address: req.body.address,
    //   email: req.body.email,
    //   role: req.body.role,
    //   status: req.body.status,
    //   password: req.body.password,
    //   image: req.file.path
    // });
  } catch (ex) {
    res.status(500).send({
      status: "failed",
      error: ex
    });
  }
});

router.put("/:id", upload.single("image"), async (req, res) => {
  let a = await User.findById(req.params.id);
  console.log(a.image);
  if (a.image != req.body.path) {
    try {
      fs.unlink("./" + a.image, function(er) {
        // console.log(er);
      });
    } catch (ex) {
      console.log({ error: ex });
    }
  }

  const { error } = validate(req.body);
  if (error) return res.status(400).send({ error: "error.details[0].message" });

  if (req.file) {
    const user = await User.findByIdAndUpdate(
      req.params.id,
      {
        fullName: req.body.fullName,

        phoneNo: req.body.phoneNo,
        userName: req.body.userName,

        dob: req.body.dob,
        address: req.body.address,
        email: req.body.email,
        role: req.body.role,
        status: req.body.status,
        password: req.body.password,
        image: req.file.path
      },
      { new: true }
    );
    if (!user)
      res.status(400).send({ message: "The user with given id is not found" });
    res.send({ message: "sucess", user: user });
  } else {
    const user = await User.findByIdAndUpdate(
      req.params.id,
      {
        fullName: req.body.fullName,

        phoneNo: req.body.phoneNo,
        userName: req.body.userName,

        dob: req.body.dob,
        address: req.body.address,
        email: req.body.email,
        role: req.body.role,
        status: req.body.status,
        password: req.body.password,
        image: ""
      },
      { new: true }
    );
    if (!user)
      res.status(400).send({ message: "The user with given id is not found" });
    res.send({ message: "sucess", user: user });
  }
});

router.delete("/:id", async (req, res) => {
  let a = await User.findById(req.params.id);
  console.log(a.image);
  try {
    fs.unlink("./" + a.image, function(er) {
      // if (er) throw er;
    });
  } catch (ex) {
    console.log(ex);
  }

  console.log("deleting");

  const user = await User.findByIdAndRemove(req.params.id);

  if (!user)
    return res
      .status(404)
      .send({ error: "The user with the given ID was not found." });

  res.send({ message: "Sucess" });
});

module.exports = router;
