const express = require("express");
const router = express.Router();
const {addRoute, getRoute, updateRoute, getBusByDistrict, getRouteById, deleteRoute, getDistrict} = require("../controllers/routes");


router.get("/dis", getDistrict)

router.get("/", getRoute)

router.post("/", addRoute)

router.get("/by/district", getBusByDistrict)

router.put("/:id", updateRoute)

router.get("/:id", getRouteById)



router.delete("/:id", deleteRoute)


module.exports = router;
