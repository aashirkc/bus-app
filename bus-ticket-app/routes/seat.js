const express = require("express");
const router = express.Router();
const {addSeat, getSeats} = require("../controllers/seats");
const { route } = require("./auth");

router.get("/", getSeats)


router.post("/", addSeat)


module.exports = router;
