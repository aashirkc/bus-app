const Seat = require('../model/seat')

exports.addSeat = async (req,res) => {
    let value = req.body
    let data = await Seat.create(value)
    res.status(200).send(
         { message: "Success" ,
          payload: data
        });
}

exports.getSeats = async(req, res) => {
    const seat = await Seat.find()
    res.status(200).json({message:"Success",payload:seat});

}