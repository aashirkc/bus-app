const Route = require('../model/routes')
const nepalGeojson = require('nepal-geojson');
const Bus = require('../model/bus')

exports.addRoute = async (req, res) => {
    let value = req.body
    let data = await Route.create(value)
     res.status(200).send(
         { message: "Success" ,
          payload: data
        });
}

exports.getRoute = async (req,res) => {
    const route = await Route.find()
    res.status(200).send({message:"Success",payload:route});
}

exports.updateRoute = async(req,res) => {
    let data = await Route.findOneAndUpdate(
       {_id: req.params.id}, 
       {$set: req.body}, 
       {new: true})
    res.status(200).send(
         { message: "Success" ,
          payload: data
        });
}


exports.getRouteById = async(req,res) => {
      let value = req.params.id;
    let data = await Route.findOne({_id: value})
     res.status(200).send(
         { message: "Success" ,
          payload: data
        });
}

exports.deleteRoute = async(req, res) => {
    const route = await Route.findByIdAndRemove(req.params.id);
    if (!route)
    return res.status(404).send("The route with the given ID was not found.");
    res.status(200).send({ message: "Success" });
}

exports.getDistrict = async(req,res) => {
  let districtsGeojson = await nepalGeojson.districts(); 
  res.status(200).json({ message: "Success", payload: districtsGeojson});
}

exports.getBusByDistrict = async(req, res) => {
  let startDistrict = req.query.From;
  let endDistrict = req.query.To;
  let route = await Route.findOne({startDistrict: startDistrict, endDistrict:endDistrict})
  if(route){
   
    res.status(200).json({ message: "Success", payload: route});
  }else{
    res.status(200).json({ message: "Success", payload: ''});
  }
}

