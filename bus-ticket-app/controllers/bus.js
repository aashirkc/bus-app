const Bus = require("../model/bus")
const Route = require("../model/routes")
const formidable = require('formidable')
const fs = require('fs')

exports.addBus = async(req, res) => {
   let form = formidable({ multiples: true })
    form.parse(req,async (err, fields, files) => {
        if(err) {
            return res.status(400).json({
                status: "failed",
                err:err,
                message: "image cannot be uploaded"
            })
        }
        let bus = await new Bus(fields)

        if(files.images) {
         
            if(files.images.size > 5000000){
               return res.status(400).json({
                   status: 400,
                   message: "Image size must be less than 5mb"
               })
            }

        
            bus.images.data = fs.readFileSync(files.images.path)
            bus.images.contentType = files.images.type
        }

        let result = await bus.save()
            result.images = undefined
            res.json({
            status: "Success",
            message: "bus added Successfully,",
            payload: result

        })
        
        
    })

}


exports.getBusses = async(req, res) => {
    const bus = await Bus.find().populate('seatType').populate('routeName').select('-images');
    res.status(200).json({
        status: "Success",
        payload: bus
    })
}

exports.photo = async(req, res, next) => {
    let busId = req.params.id
    let data = await Bus.findById(busId) 
    if(data.images.data){
         res.set('Content-Type', data.images.data.contentType)
        return res.send(data.images.data);
    }
    next();
}


exports.getBusByRoute = async(req,res) => {
    let routeId = req.params.id;
    let data = await Bus.find({routeName: routeId}).populate('routeName').select('-images')
     res.status(200).json({
        status: "Success",
        payload: data
    })
}

exports.getBusById = async(req, res) => {
     let busId = req.params.id;
    let data = await Bus.findOne({_id: busId}).populate('routeName').select('-images')
     res.status(200).json({
        status: "Success",
        payload: data
    })
}

