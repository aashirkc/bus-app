const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  const token = req.header("Authorization");

  TokenArray = token.split(" ");
  if (!TokenArray[1])
    return res.status(401).send({ error: "Access denied. No token provided." });

  try {
    const decoded = jwt.verify(TokenArray[1], "jwtPrivateKey");
    req.user = decoded;
    next();
  } catch (ex) {
    res.status(400).send({ error: "Invalid token." });
  }
};
